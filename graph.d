module va11_hall_a.dialogue.graph;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.format;
import std.path;
import std.process;
import std.range;
import std.stdio;
import std.string;

import ae.utils.aa;
import ae.utils.array;
import ae.utils.path;

import gml_lsp_emu.intstr;
import gml_lsp_emu.program;

import va11_hall_a.dialogue.lib.colors;
import va11_hall_a.dialogue.lib.constants;
import va11_hall_a.dialogue.lib.graphviz;
import va11_hall_a.dialogue.lib.serialization;
import va11_hall_a.dialogue.lib.script;
import va11_hall_a.dialogue.lib.stateset;
import va11_hall_a.dialogue.lib.vars;

void main(string[] args)
{
	textColors = getColors();

	Transitions transitions;
	string[] pointFiles;

	HashSet!VarSighting varSightings;

	foreach (arg; args[1..$])
	{
		if (arg.endsWith(".points"))
		{
			auto points = load!(ExploredPoint[])(File(arg, "rb"));

			foreach (point; points)
			{
				auto pos0 = ScriptPos(point.scriptPath, point.scriptE);
				auto set1 = point.expanded;

				auto allNewGlobals = set1.getDims.filter!(dim => dim.id == newGlobalsObj).array;

				struct EdgeState
				{
					TransitionNodePtr node;
					StateSet state;
					HashSet!Value[Addr] dimsAndValues;
					// Condition[] conditions;
				}
				EdgeState*[] edges;

				auto visitor1 = StateSetVisitor(set1);
				while (visitor1.next())
				{
					auto scriptPath1 = visitor1.get(Addrs.scriptPath);
					auto scriptE1 = visitor1.get(Addrs.scriptE);
					auto pos1 = ScriptPos(scriptPath1.get!string, scriptE1.to!int32.int32);

					// Ensure transitions to Game Over states have edges.
					auto done1 = visitor1.get(Addrs.done);
					if (done1 == Done.end)
						pos1 = ScriptPos(point.scriptPath, E.end);

					writefln("%s -> %s", pos0, pos1);

					auto edge = new EdgeState;
					auto lastNode = &edge.node;

					allNewGlobals.sort!((a, b) => newGlobalsOrder(a.name) < newGlobalsOrder(b.name));
					foreach (addr; allNewGlobals)
					{
						auto value = visitor1.get(addr);
						if (value.type != Value.Type.undefined)
						{
							auto node = TransitionNodePtr(new TransitionNode);
							node.type = TransitionNode.Type.effect;
							node.effect.type = TransitionNode.Effect.Type.setVar;
							node.effect.setVar.label = formatVarAssignment(addr.name, value);
							node.effect.setVar.name = addr.name.toString();
							node.effect.setVar.value = value.formatValue();
							*lastNode = node;
							lastNode = &node.effect.next;
						}
					}

					foreach (tip; visitor1.get(Addrs.mixertips).array1)
					{
						auto node = TransitionNodePtr(new TransitionNode);
						node.type = TransitionNode.Type.effect;
						if (tip.array1.length == 4)
						{
							node.effect.type = TransitionNode.Effect.Type.mixertips;
							auto shouldpay  = tip.array1[MixerTipsArgs.shouldpay ].int16;
							auto rightdrink = tip.array1[MixerTipsArgs.rightdrink].int16;
							auto big_able   = tip.array1[MixerTipsArgs.big_able  ].int16;
							auto tipping    = tip.array1[MixerTipsArgs.tipping   ].int16;
							if (shouldpay == 0 && rightdrink == 0 && big_able == 0 && tipping == 0)
								node.effect.mixertips = "Free drink";
							else
							if (shouldpay == 1 && rightdrink == 0 && big_able == 0 && tipping == 0)
								node.effect.mixertips = "Wrong drink!";
							else
							if (shouldpay == 1 && rightdrink == 1 && big_able.among(0, 1))
								node.effect.mixertips =
									"OK, " ~
									(tipping ? format("$%d tip", tipping) : "no tip") ~
									(big_able ? "\n+ Big bonus" : "");
							else
								throw new Exception("Unknown tipping combination: " ~ tip.toString());
						}
						else
						if (tip.array1.length == 6)
						{
							node.effect.type = TransitionNode.Effect.Type.mixertips2;
							auto shouldpay   = tip.array1[MixerTips2Args.shouldpay  ].int16;
							auto rightdrink1 = tip.array1[MixerTips2Args.rightdrink1].int16;
							auto rightdrink2 = tip.array1[MixerTips2Args.rightdrink2].int16;
							auto big_able1   = tip.array1[MixerTips2Args.big_able1  ].int16;
							auto big_able2   = tip.array1[MixerTips2Args.big_able2  ].int16;
							auto tipping     = tip.array1[MixerTips2Args.tipping    ].int16;

							string htmlFor(int rightdrink, int big_able)
							{
								return rightdrink
									?
									`<TD BORDER="2" COLOR="#446655" BGCOLOR="#113322"><FONT COLOR="#CCFFDD">OK` ~
									(big_able ? `<BR/>+ Big bonus` : ``) ~
									`</FONT></TD>`
									:
									`<TD BORDER="2" COLOR="#880000" BGCOLOR="#440000"><FONT COLOR="#FFAAAA">Wrong drink!</FONT></TD>`;
							}

							enforce(shouldpay, "Not implemented");
							node.effect.mixertips2 =
								`<TABLE CELLSPACING="0" CELLPADDING="5" BORDER="0">` ~
								`<TR>` ~
								htmlFor(rightdrink1, big_able1) ~
								htmlFor(rightdrink2, big_able2) ~
								`</TR><TR>` ~
								(
									tipping
									? `<TD COLSPAN="2" BORDER="2" COLOR="#446655" BGCOLOR="#113322"><FONT COLOR="#CCFFDD">$` ~ text(tipping) ~ ` tip</FONT></TD>`
									: `<TD COLSPAN="2" BORDER="2" COLOR="#880000" BGCOLOR="#440000"><FONT COLOR="#FFAAAA">No tip</FONT></TD>`
								) ~
								`</TR></TABLE>`;
						}
						else
							assert(false, "Unknown mixertips array length");
						*lastNode = node;
						lastNode = &node.effect.next;
					}

					foreach (achievement; visitor1.get(Addrs.steamAchievements).array1)
					{
						auto node = TransitionNodePtr(new TransitionNode);
						node.type = TransitionNode.Type.effect;
						node.effect.type = TransitionNode.Effect.Type.achievement;
						node.effect.achievement = achievement.string;
						*lastNode = node;
						lastNode = &node.effect.next;
					}

					auto node = TransitionNodePtr(new TransitionNode);
					node.type = TransitionNode.Type.jump;
					node.jump.target = pos1;
					*lastNode = node;

					auto set = visitor1.currentSubset;
					set = set.set(Addrs.edgeIndex, Value(edges.length.to!int32));

					edge.state = set;
					edge.dimsAndValues = set.getDimsAndValues();
					edges ~= edge;
				}

				if (!edges)
					continue;

				auto unionOfResults = edges.fold!((set, edge) => set.merge(edge.state))(StateSet.emptySet);
				auto dimsAndValues = unionOfResults
					.getDimsAndValues()
					.byKeyValue
					.filter!(kv => varOrder(kv.key) >= 0)
					.filter!(kv => kv.value.length > 1)
					.array
					.multiSort!((a, b) => varOrder(a.key) < varOrder(b.key), (a, b) => a.key < b.key)
					.release;

				TransitionNodePtr segment(StateSet set, int depth, long costBudget)
				{
					if (costBudget <= 0)
						return TransitionNodePtr.init;

					auto edgeIndices = set.all(Addrs.edgeIndex).map!(v => v.int32);
					if (edgeIndices.length == 1)
					{
						writefln("%*s<%d: Perfect match", 2 * depth, "", costBudget);
						return edges[edgeIndices[0]].node;
					}

					// Everything below will allocate a node, cull.
					if (costBudget <= nodeCost)
						return TransitionNodePtr.init;

					set = set.reorderUsing(unionOfResults);

					static struct CacheEntry
					{
						long costBudget;
						TransitionNodePtr result;
					}
					static CacheEntry[StateSet] cache;
					if (auto pentry = set in cache)
					{
						if (pentry.result)
						{
							if (pentry.result.cost < costBudget)
							{
								// OK, reuse saved solution, which is within our budget.
								return pentry.result;
							}
							else
							{
								// A solution had been found, but it exceeds our budget.
								return TransitionNodePtr.init;
							}
						}
						else
						{
							if (pentry.costBudget >= costBudget)
							{
								// A solution had not been found with an equivalent or even more
								// generous budget, so we will certainly not find one either.
								return TransitionNodePtr.init;
							}
							else
							{
								// Continue onwards to try to find a solution with a bigger budget.
							}
						}
					}

					TransitionNodePtr bestNode;

					void tryGroups(V, S)(Addr var, V values, S subsets)
					{
						assert(values.length == subsets.length);

						auto choiceStrings = formatChoices(var, values.array);

						long baseCost = nodeCost + varCost(var) + choiceStrings.map!(s => choiceCost(var, s)).sum;
						if (baseCost >= costBudget) return;

						// Try to cull early
						long minimalCost = baseCost;
						foreach (i; 0 .. values.length)
						{
							auto subset = subsets[i];
							auto numEdges = subset.all(Addrs.edgeIndex).length;
							if (numEdges > 1)
							{
								minimalCost += nodeCost;
								if (minimalCost >= costBudget) return;
							}
						}

						auto n = TransitionNodePtr(new TransitionNode);
						n.type = TransitionNode.Type.condition;
						n.condition.addr = var;
						n.condition.label = formatVar(var) ~ "?";
						n.cost = baseCost;
						foreach (i; 0 .. values.length)
						{
							auto choice = TransitionNodePtr(new TransitionNode);
							choice.type = TransitionNode.Type.choice;
							choice.choice.label = choiceStrings[i];
							choice.choice.obtainable = isObtainable(var, choiceStrings[i]);
							auto subset = subsets[i];
							auto valuesArr = values[i].keys; writefln("%*s%s%s", 2 * depth + 1, "", valuesArr[0 .. min($, 10)], valuesArr.length > 10 ? "..." : "");
							choice.choice.next = segment(subset, depth + 1, (bestNode ? bestNode.cost : costBudget) - n.cost);
							if (!choice.choice.next) return;
							choice.cost = choice.choice.next.cost + choiceCost(var, choiceStrings[i]);
							n.condition.choices ~= choice;
							n.cost += choice.choice.next.cost; // choiceCost is already added
							assert(n.cost < costBudget);
						}
						if (bestNode)
							assert(n.cost < bestNode.cost);
						bestNode = n;
					}

					// Try to subdivide into groups where only some sets have only some values

				loop1:
					foreach (dim; dimsAndValues)
					{
						if (costBudget <= nodeCost + varCost(dim.key))
							continue;

						auto allValues = set.all(dim.key);
						if (allValues.length < 2) continue;

						struct Group
						{
							HashSet!Value values;
							HashSet!int edges;
						}
						Group[] groups;
						HashSet!Value valueVisited;
						HashSet!int edgeVisited;
						bool visit(Value value, int edgeIndex, size_t groupIndex)
						{
							if (value !in valueVisited)
							{
								valueVisited.add(value);
								if (groups.length == groupIndex) groups.length = groupIndex + 1;
								groups[groupIndex].values.add(value);
								auto valueEdges = set.getAllFor(dim.key, value, Addrs.edgeIndex);
								if (valueEdges.length == edgeIndices.length) return false; // auto-fail - 1 group guaranteed
								foreach (edgeIndex2; valueEdges.byKey.map!(v => v.int32))
									if (!visit(value, edgeIndex2, groupIndex))
										return false;
							}
							if (edgeIndex !in edgeVisited)
							{
								edgeVisited.add(edgeIndex);
								if (groups.length == groupIndex) groups.length = groupIndex + 1;
								groups[groupIndex].edges.add(edgeIndex);
								auto edgeValues = set.getAllFor(Addrs.edgeIndex, Value(edgeIndex), dim.key);
								if (edgeValues.length == allValues.length) return false; // auto-fail - 1 group guaranteed
								foreach (value2; edgeValues)
									if (!visit(value2, edgeIndex, groupIndex))
										return false;
							}
							return true;
						}

						foreach (edgeIndex; set.all(Addrs.edgeIndex))
							foreach (value; set.getAllFor(Addrs.edgeIndex, edgeIndex, dim.key))
								if (!visit(value, edgeIndex.int32, groups.length))
									continue loop1;
						writefln("%*s<%d: 1/%s - %d edges, %d values, %d groups, %d values here", 2 * depth, "", (bestNode ? bestNode.cost : costBudget), dim.key, edgeIndices.length, dim.value.length, groups.length, allValues.length);

						if (groups.length < 2) continue;

						tryGroups(dim.key,
							groups.map!((ref g) => g.values),
							groups.map!((ref g) => g.edges.fold!((s, edgeIndex) => s.merge(set.get(Addrs.edgeIndex, Value(edgeIndex))))(StateSet.emptySet)),
						);
					}

					// The above failed, which means subdividing by exact value groups isn't possible.
					// E.g., due to the condition being an "and" of multiple subconditions.
					// Try to subdivide by anything that further fragments edge indices, even if there's overlap.

					foreach (dim; dimsAndValues)
					{
						if (costBudget <= nodeCost + varCost(dim.key))
							continue;

						auto allValues = set.all(dim.key);
						if (allValues.length < 2) continue;

						HashSet!Value[int[]] edgeGroups;
						foreach (value; allValues)
							edgeGroups.require(
								set.getAllFor(dim.key, value, Addrs.edgeIndex).byKey.map!(v => int(v.int32)).array.sort.release,
								HashSet!Value.init
							).add(value);
						if (edgeGroups.length < 2) continue;
						auto groups = edgeGroups.byKeyValue.array;

						writefln("%*s<%d: 2/%s - %d edges, %d values, %d groups, %d values here", 2 * depth, "", (bestNode ? bestNode.cost : costBudget), dim.key, edgeIndices.length, dim.value.length, groups.length, allValues.length);
						tryGroups(
							dim.key,
							groups.map!((ref kv) => kv.value),
							groups.map!((ref kv) => kv.value.byKey.fold!((s, value) => s.merge(set.get(dim.key, value)))(StateSet.emptySet)),
						);
					}

					// We should have found something by this point.
					// If not, subdivision (at least using this subset) is likely impossible.

					cache[set] = CacheEntry(costBudget, bestNode);

					return bestNode;
				}

				auto n = segment(unionOfResults, 0, 1_000_000_000);
				if (!n)
				{
					unionOfResults.save(File("subdivision-failure.set", "wb"));
					throw new Exception("Failed to subdivide :(");
				}
				writeln("Found solution with cost: ", n.cost);

				void scan(TransitionNodePtr n)
				{
					final switch (n.type)
					{
						case TransitionNode.Type.condition:
							if (n.condition.addr.id == globalObj)
								useGlobal(n.condition.addr.name);
							if (n.condition.addr.id == backupObj)
								varSightings.add(VarSighting(point.scriptPath, point.scriptE, n.condition.addr.name, false));
							foreach (choice; n.condition.choices)
								scan(choice);
							break;
						case TransitionNode.Type.choice:
							scan(n.choice.next);
							break;
						case TransitionNode.Type.effect:
							if (n.effect.type == TransitionNode.Effect.Type.setVar)
								varSightings.add(VarSighting(point.scriptPath, point.scriptE, IntStr(n.effect.setVar.name), true));
							scan(n.effect.next);
							break;
						case TransitionNode.Type.jump:
							break;
					}
				}
				scan(n);

				auto orders = unionOfResults.all(Addrs.orderDesc);
				if (orders[0] == Value.undefined)
					transitions[pos0] ~= n;
				else
					foreach (order; orders)
					{
						if (order.string.startsWith("(random distracted order description"))
							continue;
						auto node = TransitionNodePtr(new TransitionNode);
						node.type = TransitionNode.Type.effect;
						node.effect.type = TransitionNode.Effect.Type.orderDesc;
						node.effect.orderDesc = order.string.replace("#", "\n").wrap(50);
						node.effect.next = n;
						transitions[pos0] ~= node;
					}
			}

			pointFiles ~= arg.stripExtension;
		}
		else
		if (arg.endsWith(".transitions"))
		{
			auto fileTransitions = load!Transitions(File(arg, "rb"));
			foreach (k, v; fileTransitions)
				transitions[k] ~= v;
		}
		else
			throw new Exception("Unknown extension: " ~ arg);
	}

	if (pointFiles)
	{
		auto fn = pointFiles.join("+") ~ ".transitions";
		stderr.writeln("Saving ", fn);
		transitions.save(File(fn, "wb"));
	}

	saveSightings(varSightings);

	Pid[] procs;
	auto allScripts = transitions.byKey.map!(pos => pos.scriptPath).array.sort.uniq;
	string[] allDots;
	foreach (verbose; [false, true])
		foreach (scriptPath; allScripts)
		{
			stderr.writeln(scriptPath);
			auto dots = dump(scriptPath, transitions, verbose);
			foreach (dot; dots)
			{
				procs ~= spawnProcess(["dot", "-Tsvg", "-O", dot]);
				procs ~= spawnProcess(["dot", "-Tpng", "-O", dot]);
			}
			if (!verbose)
				allDots ~= dots;
		}
	foreach (proc; procs)
		enforce(proc.wait() == 0);
	foreach (dot; allDots)
	{
		auto svg = dot ~ ".svg";
		writefln("%s: %s", svg, svg
			.readText
			.findSplit(`<polygon fill="transparent" stroke="transparent" points="`)[2]
			.findSplit(`"`)[0]
			.split(" ")[2]
			.findSplit(",")[0]
		);
	}
}

long varOrder(Addr var)
{
	// Try the more obvious variables first, to ensure quick culling
	static Addr[] goodVars; if (!goodVars) goodVars = [
		backup!      "bev_a", backup!      "bev_b",
		backup!"drinksize_a", backup!"drinksize_b",
		backup!   "flavor_a", backup!   "flavor_b",
		backup!     "kind_a", backup!     "kind_b",
		backup!  "alcohol_a", backup!  "alcohol_b",
		backup!      "otr_a", backup!      "otr_b",
		backup!"drunklevel_a",
		global!"ngplus_flag",
	];
	auto i = goodVars.indexOf(var);
	if (i >= 0)
		return 1 + i;

	if (var.id.among(backupObj))
		return 500;
	if (var.id.among(newGlobalsObj, steamAchievementsObj))
		return -1;
	if (var.id == globalObj && var.name in globalsToBackup)
		return -1; // Use the backup instead!

	if (var.among(
		Addrs.scriptE,
		Addrs.scriptPath,
		Addrs.origScriptE,
		Addrs.origScriptPath,
		Addrs.edgeIndex,
		Addrs.mixertips,
		Addrs.annaChoiceNum,
		Addrs.annaChoicesCopy,
		Addrs.done,
		Addrs.runMode,
		Addrs.textboxCreated,
	))
		return -1;

	return 1000;
}

long varCost(Addr var)
{
	enum multiplier = 10;

	static Addr[] goodVars; if (!goodVars) goodVars = [
		backup!   "flavor_a", backup!   "flavor_b",
		backup!     "kind_a", backup!     "kind_b",
		backup!  "alcohol_a", backup!  "alcohol_b",
		backup!      "bev_a", backup!      "bev_b",
		backup!"drinksize_a", backup!"drinksize_b",
		backup!      "otr_a", backup!      "otr_b",
		backup!"drunklevel_a",
		Addrs.annaChoiceStr,
		global!"ngplus_flag",
	];
	auto i = goodVars.indexOf(var);
	if (i >= 0)
		return (1 + i) * multiplier;

	enum badCost = nodeCost * 100;

	static IntStr[] badVars; if (!badVars) badVars = [
		intstr!"orders",
		intstr!"drunklevel_a",
		intstr!"mixhappens",
		intstr!"slotamount",

		intstr!"shouldpay",
		intstr!"rightdrink", intstr!"rightdrink1", intstr!"rightdrink2",
		intstr!"big_able"  , intstr!"big_able1"  , intstr!"big_able2"  ,
		intstr!"tipping",

		intstr!"almaend",
		intstr!"badend",
		intstr!"concertend",
		intstr!"curryend",
		intstr!"doroend",
	];
	if (var.id.among(globalObj, backupObj))
	{
		i = badVars.indexOf(var.name);
		if (i >= 0)
			return badCost + i;
	}

	if (var.id.among(globalObj, backupObj))
		return 500;

	return 1000;
}

enum nodeCost = 10000;

long choiceCost(Addr var, string s)
{
	long cost = s.length;

	// Work around typo (inconsistent case) in game code
	if (var.among(backup!"flavor_a", backup!"flavor_b") && s.among(`"Sweet"`, `"Bitter"`, `"ssour"`))
		cost += nodeCost * 100;

	// Penalize big nodes
	cost += (s.splitLines.length - 1) * nodeCost / 3;

	return cost;
}

int newGlobalsOrder(IntStr name)
{
	int order = name.index;
	// Put these up front to reduce number of duplicate nodes
	// (when another variable is assigned within the same branch)
	if (name.among(intstr!"ft_alma", intstr!"ft_stella"))
		order -= 1_000_000;
	return order;
}

string formatVar(Addr var)
{
	if (var == backup!   "flavor_a") return "Flavor of\nBeverage A";
	if (var == backup!   "flavor_b") return "Flavor of\nBeverage B";
	if (var == backup!     "kind_a") return   "Kind of\nBeverage A";
	if (var == backup!     "kind_b") return   "Kind of\nBeverage B";
	if (var == backup!  "alcohol_a") return            "Beverage A\nhas alcohol";
	if (var == backup!  "alcohol_b") return            "Beverage B\nhas alcohol";
	if (var == backup!      "otr_a") return            "Beverage A\nhas ice";
	if (var == backup!      "otr_b") return            "Beverage B\nhas ice";
	if (var == backup!      "bev_a") return            "Beverage A";
	if (var == backup!      "bev_b") return            "Beverage B";
	if (var == backup!"drinksize_a") return   "Size of\nBeverage A";
	if (var == backup!"drinksize_b") return   "Size of\nBeverage B";
	if (var == backup!"drunklevel_a") return "Total\nKarmotrine\nused";
	if (var == backup!"housedl") return "Rent\npaid";
	if (var == backup!"ngplus_flag") return "New Game +";
	if (var == Addrs.totalIngredients) return "Total\ningredients";
	if (var == Addrs.todPortrait) return "Pick\nwho";
	if (var == Addrs.annaChoiceStr) return "Picked\ndialogue\nchoice";
	if (var.id == backupObj)
		return format("Value of\n%s", var.name);
	return var.text;
}

string formatVarAssignment(IntStr varName, Value value)
{
	if (varName == intstr!"drunklevel_a" && value.to!int32.int32 == 0) return "Reset total\nKarmotrine\nused";
	return null;
}

string[] formatChoices(Addr var, HashSet!Value[] choices)
{
	if (var.among(backup!"otr_a", backup!"otr_b"))
		return choices.amap!(choiceSet =>
			choiceSet.keys.sort.map!(choice =>
				["No", "Yes"][choice.int16]
			).join(" or\n")
		);

	if (var == Addrs.todPortrait)
		return choices.amap!(choiceSet =>
			choiceSet.keys.sort.map!(choice =>
				[
					"Alma",
					"Dana",
					"Dorothy",
					"Gillian",
					"Rad Shiba",
				][choice.int16]
			).join(" or\n")
		);

	auto result = new string[choices.length];

	foreach (i, ref values; choices)
	{
		if (values.length == 1)
			result[i] = values.byKey.front.formatValue;
		else
		{
			auto range = values.keys.sort;

			if (!collectException(enforce(equal(range, range.length.iota.map!(i => range[0] + Value(cast(int16)i))))))
				result[i] = format("%s to %s", formatValue(range[0] + Value(int16(0))), formatValue(range[$-1]));
			else
				result[i] = format("%-(%s or\n%)", range.map!formatValue);
		}
		if (result[i] == `"Failed drink (unobtainable)"`)
			result[i] = "Failed drink\n(unobtainable)";
		if (var == Addrs.annaChoiceStr && result[i] == "undefined")
			result[i] = "(not asked)";
	}

	if (!var.among(Addrs.annaChoiceStr))
	{
		auto longestChoice = choices.length.iota.fold!((i, j) => result[i].length > result[j].length ? i : j);
		if (result[longestChoice].length > "otherwise".length)
			result[longestChoice] = "otherwise";
	}

	return result;
}

bool isObtainable(Addr var, string choiceStr)
{
	if (choiceStr == "Failed drink\n(unobtainable)")
		return false;
	return true;
}

string formatValue(Value value)
{
	string s = value.toString();
	if (value.type == Value.Type.int16)
		s = s.chomp("s");
	if (value.type == Value.Type.double_)
		s = s.chomp("d");
	return s;
}
