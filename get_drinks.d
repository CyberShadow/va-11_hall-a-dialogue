module va11_hall_a.dialogue.get_drinks;

import ae.utils.array;
import ae.utils.json;

import std.algorithm.iteration;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.datetime.stopwatch;
import std.range;
import std.stdio;
import std.traits;

import va11_hall_a.dialogue.lib.game;
import va11_hall_a.dialogue.lib.stateset;
import va11_hall_a.dialogue.lib.serialization;
import va11_hall_a.dialogue.lib.vars;

import gml_lsp_emu;

StateSet expandDrinks(string which)()
{
	auto game = new VisitorGame();

	{
		auto set =
			StateSet.emptySet
			.merge(
				StateSet.unitSet
				.cartesianProduct(global!("exdrink_" ~ which), ["abs", "rum", "tea", "fed", "fail"].amap!(name => Value(name)))
				.set             (global!("mod_a"    ~ which), Value(0.int16))
				.set             (global!("mod_b"    ~ which), Value(0.int16))
				.set             (global!("mod_c"    ~ which), Value(0.int16))
				.set             (global!("mod_d"    ~ which), Value(0.int16))
				.set             (global!("mod_e"    ~ which), Value(0.int16))
				.set             (global!("otr_"     ~ which), Value(0.int16))
				.set             (global!("age_"     ~ which), Value(0.int16))
				.set             (global!("mixed_"   ~ which), Value(0.int16))
				.set             (global!("failed_"  ~ which), Value(0.int16))
			)
			.merge(
				StateSet.unitSet
				.set             (global!("exdrink_" ~ which), Value(""))
				.cartesianProduct(global!("mod_a"    ~ which), 21.int16.iota.map!(n => Value(n)).array)
				.cartesianProduct(global!("mod_b"    ~ which), 21.int16.iota.map!(n => Value(n)).array)
				.cartesianProduct(global!("mod_c"    ~ which), 21.int16.iota.map!(n => Value(n)).array)
				.cartesianProduct(global!("mod_d"    ~ which), 21.int16.iota.map!(n => Value(n)).array)
				.cartesianProduct(global!("mod_e"    ~ which), 21.int16.iota.map!(n => Value(n)).array)
				.cartesianProduct(global!("otr_"     ~ which), [Value(0.int16), Value(1.int16)])
				.cartesianProduct(global!("age_"     ~ which), [Value(0.int16), Value(1.int16)])
				.cartesianProduct(global!("mixed_"   ~ which), [Value(0.int16), Value(1.int16)])
				.cartesianProduct(global!("failed_"  ~ which), [Value(0.int16), Value(1.int16)])
			)
		;
		static immutable vars = ["cur_day", "cur_stage", "cur_client"];
		static foreach (var; vars)
			set = set.cartesianProduct(global!var   , getVarValues(intstr!var).keys.sort.release);

		stderr.writefln("Trying %d drink combinations.", set.count);
		game.load(set);
	}
	StopWatch sw; sw.start();

	auto drinkSpriteID = game.env.allocObject();
	game.env.addGlobalObject(intstr!"drinksprite_a", drinkSpriteID);
	game.env.addGlobalObject(intstr!"drinksprite_b", drinkSpriteID);

	StateSet drinks;
	uint iterations;
	while (game.next())
	{
		iterations++;
		bool ok;
		if (game.get(global!("exdrink_" ~ which)).string == "fail")
		{
			// Fake "fail" drink for Anna
			game.put(global!("exdrink_"    ~ which), Value(""));
			game.put(global!("bev_"        ~ which), Value("Failed drink (unobtainable)"));
			game.put(global!("bevid_"      ~ which), Value("failed"));
			game.put(global!("kind_"       ~ which), Value("fail"));
			game.put(global!("flavor_"     ~ which), Value("fail"));
			game.put(global!("drinksize_"  ~ which), Value("normal"));
			game.put(global!("alcohol_"    ~ which), Value("no"));
			game.put(global!("drinkscore_" ~ which), Value(0.int16));
			ok = true;
		}
		else
		{
			game.runFunction("gml_Script_drink_" ~ which, null, []);
			ok = game.get(global!("bevid_" ~ which)).string != "failed";
		}
		if (ok)
		{
			auto total =
				game.get(global!("mod_a" ~ which)).int16 +
				game.get(global!("mod_b" ~ which)).int16 +
				game.get(global!("mod_c" ~ which)).int16 +
				game.get(global!("mod_d" ~ which)).int16 +
				game.get(global!("mod_e" ~ which)).int16;
			if (total > 20)
				continue;

			game.put(Addrs.totalIngredients, Value(total));

			// The game will quickly clobber the mixer variables.
			// Save what drink we brewed, and its properties, to a private namepace here.
			static foreach (prefix; ["bev_", "flavor_", "kind_", "drinksize_", "alcohol_", "otr_"])
				game.put(backup!(prefix ~ which), game.get(global!(prefix ~ which)));

			auto result = game.save();
			result = result.remove(Addr(drinkSpriteID, intstr!"spriteshown"));
			drinks = drinks.merge(result);
		}
	}

	stderr.writefln("Found %d outcomes in %s and %d iterations.", drinks.count, sw.peek(), iterations);

	return drinks;
}

void main()
{
	initGame();

	auto drinksA = expandDrinks!"a"().optimize();
	auto drinksB = expandDrinks!"b"().optimize();

	drinksA.save(File("drinks-a.set", "wb"));
	drinksB.save(File("drinks-b.set", "wb"));

	// Needed to allow resetmixer_2 to do its thing and reduce the number of states.
	foreach (var; drinksA.merge(drinksB).getDims)
		if (var.id == globalObj)
		{
			checkRead(var);
			checkWrite(var);
		}
}
