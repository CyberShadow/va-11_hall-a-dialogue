module va11_hall_a.dialogue.graph_set;

import std.exception;
import std.process;
import std.stdio;

import va11_hall_a.dialogue.lib.serialization;
import va11_hall_a.dialogue.lib.stateset;

void main(string[] args)
{
	Pid[] procs;
	foreach (fn; args[1..$])
	{
		auto set = load!StateSet(File(fn));
		auto dot = fn ~ ".dot";
		dumpStateSetGraph(set, File(dot, "wb"));
		procs ~= spawnProcess(["dot", "-Tsvg", "-O", dot]);
	}
	foreach (proc; procs)
		enforce(proc.wait() == 0);
}
