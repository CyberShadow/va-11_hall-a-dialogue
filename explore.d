module va11_hall_a.dialogue.explore;

import ae.utils.aa;
import ae.utils.meta;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.datetime.stopwatch;
import std.exception;
import std.file;
import std.format;
import std.path;
import std.stdio : stderr, File;

import gml_lsp_emu.intstr;
import gml_lsp_emu.program;

import va11_hall_a.dialogue.lib.expansion;
import va11_hall_a.dialogue.lib.game;
import va11_hall_a.dialogue.lib.script : E;
import va11_hall_a.dialogue.lib.serialization;
import va11_hall_a.dialogue.lib.stateset;
import va11_hall_a.dialogue.lib.vars;

void expandSegment(string fileName)
{
	stderr.writefln("========== %s ==========", fileName);

	auto seed = load!StateSet(File(fileName));
	mixin(mixScopeSet!q{seed});

	stderr.writef("Exploring "); stderr.flush();
	stderr.writef("%d states " , seed.bigCount      ); stderr.flush();
	stderr.writef("(%d nodes, ", seed.uniqueNodes   ); stderr.flush();
	stderr.writef("%d dims"    , seed.getDims.length); stderr.flush();
	stderr.writeln(").");

	StopWatch sw; sw.start();
	StateSet all = expandAll(seed);

	stderr.writef("Explored "); stderr.flush();
	stderr.writef("%d states " , all.bigCount      ); stderr.flush();
	stderr.writef("(%d nodes, ", all.uniqueNodes   ); stderr.flush();
	stderr.writef("%d dims"    , all.getDims.length); stderr.flush();
	stderr.writef(") in %s", sw.peek);
	stderr.writef(" and %d kB RSS", "/proc/self/statm".readText().split[1].to!int * 4);
	stderr.writef(  " / %d kB VM" , "/proc/self/statm".readText().split[0].to!int * 4);
	stderr.writeln(".");

	// Save seeds for next segment
	{
		auto doneSync = all.get(Addrs.done, Done.sync);
		auto doneEnd  = all.get(Addrs.done, Done.end );

		if (doneSync !is StateSet.emptySet)
		{
			auto visitor = StateSetVisitor(doneSync);
			string[] positions;
			while (visitor.next())
			{
				auto day    = visitor.get(global!"cur_day"   ).to!int16.int16;
				auto stage  = visitor.get(global!"cur_stage" ).to!int16.int16;
				auto client = visitor.get(global!"cur_client").to!int16.int16;
				positions ~= format("%d,%d,%d", day, stage, client);
			}
			assert(positions.length > 0);
			if (positions.length > 1)
				stderr.writeln("WARNING: more than one final day/stage/client at sync, desynchronized?");
			doneSync = doneSync.set(Addrs.done, Done.no);
			auto fileName2 = format("state_%-(%s+%).set", positions);
			stderr.writef("Saving %s ... ", fileName2); stderr.flush();
			enforce(fileName2 != fileName, "Stuck? Not overwriting input.");
			debug {} else doneSync = doneSync.optimize3();
			doneSync.save(File(fileName2, "wb"));
			stderr.writeln();
			File("states.txt", "ab").writeln(fileName2);
		}

		if (doneEnd !is StateSet.emptySet)
		{
			enforce(doneSync is StateSet.emptySet, "Some states reached checkpoint, some didn't?");
			stderr.writeln("That's all, folks! All states reached Game Over.");
			foreach (var; persistentVars)
			{
				checkRead(var);
				foreach (value; doneEnd.all(var))
					if (value.type != Value.Type.undefined)
						checkVarValue(var.name, value);
			}
		}
	}

	// Save points for graphing
	{
		ExploredPoint[] points;
		mixin(mixScopeSet!q{all});

		auto visitor0 = StateSetVisitor(all);
		while (visitor0.next())
		{
			if (visitor0.get(Addrs.runMode) != RunMode.code)
				continue;

			auto scriptPath0 = visitor0.get(Addrs.scriptPath);
			auto scriptE0 = visitor0.get(Addrs.scriptE);
			if (visitor0.get(Addrs.done) != Done.no)
				continue;

			auto set0 = visitor0.currentSubset;
			set0 = set0.optimize();
			mixin(mixScopeSet!q{set0});
			clearStateSetCache();
			scope(success) stderr.writeln();
			stderr.writef("Re-expanding %s @ %2s: ", scriptPath0, scriptE0); stderr.flush();
			stderr.writef("%d states (%d nodes, %d dims) -> ", set0.bigCount, set0.uniqueNodes, set0.getDims.length); stderr.flush();
			auto set1 = expand!false(set0).optimize();
			stderr.writef("%d states (%d nodes, %d dims)"    , set1.bigCount, set1.uniqueNodes, set1.getDims.length); stderr.flush();

			if (scriptPath0.type == Value.Type.undefined && scriptE0.type == Value.Type.undefined)
			{
				// Create a fake "game start" point, to allow generating
				// a decision tree for where the game should start.
				scriptPath0 = set1.all(Addrs.scriptPath)[0];
				// Hack: create a second "start" node for Anna
				if (seed.all(global!"cur_day"   ) == [Value(0.int16)] &&
					seed.all(global!"cur_stage" ) == [Value(0.int16)] &&
					seed.all(global!"cur_client") == [Value(1.int16)])
					scriptE0 = Value(E.start2);
				else
					scriptE0 = Value(E.start);
			}

			ExploredPoint point;
			point.scriptPath = scriptPath0.get!string;
			point.scriptE = scriptE0.to!int32.int32;
			point.expanded = set1;
			liveSets[set1]++;
			points ~= point;
		}

		if (seed.all(global!"cur_day") == [Value(20.int16)])
		{
			// Some special logic here to aid graphing the endings.

			struct ScriptPos { string path; int e; }
			bool[ScriptPos][ScriptPos] reachable;
			HashSet!ScriptPos allPositions;

			bool isReachable(ScriptPos p0, ScriptPos p1) { return reachable.get(p0, null).get(p1, false); }
			bool isOrIsReachable(ScriptPos p0, ScriptPos p1) { return p0 == p1 || isReachable(p0, p1); }

			ScriptPos pos0Of(ref StateSetVisitor v) { return ScriptPos(v.get(Addrs.origScriptPath).string, v.get(Addrs.origScriptE).to!int32.int32); }
			ScriptPos pos1Of(ref StateSetVisitor v) { return ScriptPos(v.get(Addrs.    scriptPath).string, v.get(Addrs.    scriptE).to!int32.int32); }

			StateSet clean(StateSet set)
			{
				return set
					.remove(var => var.id.among(globalObj, backupObj) && var.name.among(
						intstr!"cur_stage"
					))
					.set(Addrs.mixertips , Value(Value.Type.array1))
				;
			}

			foreach (ref point; points)
			{
				auto pos0 = ScriptPos(point.scriptPath, point.scriptE);
				auto visitor1 = StateSetVisitor(point.expanded);
				while (visitor1.next())
				{
					auto scriptPath1 = visitor1.get(Addrs.scriptPath);
					auto scriptE1    = visitor1.get(Addrs.scriptE);
					auto pos1 = ScriptPos(scriptPath1.string, scriptE1.int16);
					if (visitor1.get(Addrs.done) == Done.end)
						pos1.e = E.end;
					reachable[pos0][pos1] = true;
					allPositions.add(pos0);
					allPositions.add(pos1);
				}
			}

			// Construct full reachability matrix
			{
				bool changed = true;
				while (changed)
				{
					changed = false;
					foreach (p0; allPositions)
					foreach (p1; allPositions)
					foreach (p2; allPositions)
						if (!isReachable(p0, p2) &&
							isReachable(p0, p1) &&
							isReachable(p1, p2))
						{
							reachable[p0][p2] = true;
							changed = true;
						}
				}
			}

			// Sort in visit order
			auto positionList = allPositions.keys.sort!((a, b) =>
				isReachable(a, b) ? true :
				isReachable(b, a) ? false :
				a.e < b.e
			);

			// Print a nice plot
			{
				stderr.writeln();
				//int order(int e) { return e == 9 ? 0 : e == E.end ? 9 : e; }
				//auto positionList = positions.keys.sort!((a, b) => order(a.e) < order(b.e));
				stderr.writeln(positionList);
				foreach (p0; positionList)
				{
					foreach (p1; positionList)
						stderr.write(reachable.get(p0, null).get(p1, false) ? "X" : ".");
					stderr.writeln;
				}
			}

			// Construct full union of expanded sets, while recording their origins
			// Note: we treat this as a set of edges in the algorithms below.
			StateSet allExpanded;
			foreach (ref point; points)
			{
				auto set = point.expanded;
				set = set.set(Addrs.origScriptPath, Value(point.scriptPath));
				set = set.set(Addrs.origScriptE   , Value(point.scriptE   ));
				allExpanded = allExpanded.merge(set);
			}
			allExpanded = allExpanded.remove(dim => dim.id == newGlobalsObj);
			stderr.writeln(allExpanded.bigCount);

			points = null;
			short counter = -1000;
			ScriptPos visit(ScriptPos pos0, StateSet set0)
			{
				// Get immediate neighbors
				auto positions1 = positionList
					.filter!(pos1 =>
						// Can reach pos1 from pos0
						isReachable(pos0, pos1)
						// but only directly (there does not exist any other
						// position through which we can reach pos1)
						&& !positionList.any!(posM => isReachable(pos0, posM) && isReachable(posM, pos1))
					)
					.array;
				stderr.writefln("%s => %s (%d states)", pos0, positions1, set0.bigCount);

				if (positions1.length > 1)
				{
					StateSet result;
					foreach (pos1; positions1)
					{
						auto set1 = set0.filter!(v => isOrIsReachable(pos1, pos1Of(*v)));
						auto pos1p = visit(pos1, set1);
						stderr.writefln("  -> %s (%d states)", pos1p, set1.bigCount);
						result = result.merge(set1
							// .set(Addrs.origScriptPath, Value(pos0 .path))
							// .set(Addrs.origScriptE   , Value(pos0 .e   ))
							.set(Addrs.    scriptPath, Value(pos1p.path))
							.set(Addrs.    scriptE   , Value(pos1p.e   ))
							.set(Addrs.done, pos1p.e == E.end ? Done.end : Done.no)
							.I!clean
						);
					}
					auto pos0p = ScriptPos(positions1[0].path, counter++);
					points ~= ExploredPoint(pos0p.path, pos0p.e, result);
					return pos0p;
				}
				else
				if (positions1.length == 1)
				{
					auto pos1 = positions1[0];
					auto pos1p = visit(pos1, set0);

					// Original node -> next node
					points ~= ExploredPoint(
						pos0.path, pos0.e,
						StateSet.unitSet
						.set(Addrs.    scriptPath, Value(pos1p.path))
						.set(Addrs.    scriptE   , Value(pos1p.e   ))
						.I!clean
					);

					// All edges that bypass pos0
					auto skipEdges = set0.filter!(v => isReachable(pos0Of(*v), pos0) && isReachable(pos0, pos1Of(*v)));
					if (skipEdges == StateSet.emptySet)
						return pos0; // No need for a proxy node, all paths go through here

					// Proxy node
					auto pos0p = ScriptPos(pos0.path, counter++);

					// Edges from proxy node
					points ~= ExploredPoint(
						pos0p.path, pos0p.e,
						StateSet.emptySet
						.merge(
							// Edge to real node
							set0.filter!(v => pos1Of(*v) == pos0)
						)
						.merge(
							// Edge to next node
							skipEdges
							.set(Addrs.    scriptPath, Value(pos1p.path))
							.set(Addrs.    scriptE   , Value(pos1p.e   ))
							.set(Addrs.done, pos1p.e == E.end ? Done.end : Done.no)
						)
						.I!clean
					);

					return pos0p;
				}
				else
					return pos0;
			}

			// Find initial states (no edges towards them).
			foreach (pos0; positionList)
				if (positionList.all!(pos1 => !isReachable(pos1, pos0)))
					visit(pos0, allExpanded.filter!(v => isOrIsReachable(pos0, pos0Of(*v))));
		}

		points.save(File(fileName.setExtension(".points"), "wb"));
	}
}

void main(string[] args)
{
	stderr.writeln("Initializing...");
	initGame();
	loadDrinks();

	foreach (arg; args[1..$])
		expandSegment(arg);
}
