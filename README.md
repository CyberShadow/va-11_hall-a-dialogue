VA-11 Hall-A utilities
======================

Usage
-----

1. Set up a D compiler (known to work with 2.091.0)
1. Clone this repository with submodules
1. Decompile `game.win` / `game.unx` with [Altar.NET](https://gitlab.com/PoroCYon/Altar.NET) (known to work with [this build](https://gitlab.com/PoroCYon/Altar.NET/-/jobs/276540923/artifacts/download)), and place the resulting output in the `decompiled/` directory
1. Copy or symlink the game's `scripts` directory to this directory
1. Run `get_drinks`
1. Run `gen_seeds`
1. Run `explore` on the `state_*.set` files (doing so creates `*.points` files and more `state_*.set` files)
1. Run `graph` on the `*.points` files

Warning: map require lots of RAM.

If anything fails with "UNEXPECTED [...]" and "Var definitions updated, re-run required", re-run it and keep re-running it until it succeeds. If that doesn't help, re-run everything from the beginning. This will need to be done many times as the software keeps learning more about the game's behavior.
