* Final State

The image below is a visual representation of all the possible states that the game can be in when reaching the end of the game.

Every state is represented as a distinct way to travel from the top of the graph to the bottom; the traveled path represents the values of variables from that state.

[previewicon=final-state.png;sizeOriginal][/previewicon]
