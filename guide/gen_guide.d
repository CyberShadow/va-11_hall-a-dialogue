module va11_hall_a.dialogue.guide.gen_guide;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.array;
import std.exception;
import std.file;
import std.format;
import std.parallelism;
import std.path;
import std.process;
import std.range;
import std.regex;
import std.stdio;
import std.string;

import ae.utils.graphics.color;
import ae.utils.graphics.im_convert;
import ae.utils.graphics.image;
import ae.utils.graphics.view;
import ae.utils.meta;
import ae.utils.regex;

import va11_hall_a.dialogue.lib.vars;

string[] splitPNG(string fn, string dir)
{
	auto image = parseViaIMConvert!BGRA(read(fn));
	enum heightLimit = 10_000;
	if (image.h < heightLimit)
	{
		auto fn2 = dir.buildPath(fn.baseName);
		copy(fn, fn2);
		return [fn2];
	}

	auto numChunks = image.h / heightLimit + 1;
	auto chunkSize = image.h / numChunks;
	xy_t lastY = 0;
	string[] result;
	foreach (chunk; 0 .. numChunks)
	{
		xy_t bestY;
		if (chunk + 1 == numChunks)
			bestY = image.h;
		else
		{
			uint bestScore = 0;
			xy_t baseY = (chunk + 1) * chunkSize;
			foreach (y; baseY .. baseY + heightLimit / 10)
			{
				uint score = 0;
				foreach (ref u; image.scanline(y))
					foreach (c; u)
						if (c.a == 0)
							score++;
				if (score > bestScore)
				{
					bestScore = score;
					bestY = y;
				}
			}
		}
		auto chunkFn = format("%s/%s-chunk-%d.png", dir, fn.baseName.stripExtension, chunk);
		image.crop(0, lastY, image.w, bestY).colorMap!(c => RGBA(c.r, c.g, c.b, c.a)).toPNG.toFile(chunkFn);
		result ~= chunkFn;
		lastY = bestY;
	}
	return result;
}

void orgToSteam(string org, string steam)
{
	spawnProcess(["emacs", org,
		"--batch",
		"--load", "~/.emacs.d/opt/ox-bb/ox-bb.el".expandTilde,
		"-f", "org-bb-export-to-bbcode", "--kill",
	]).wait.I!(pid => enforce(pid == 0, "ox-bb failed"));

	auto bb = org.setExtension(".bbcode");
	bb
		.readText
		.replace(`[font=monospace]`, ``)
		.replace(`[/font]`, ``)
		.replace("\n[list]", "[list]")
		.replace("[/u][/b]\n[list]", "[/u][/b]\n\n[list]")
		.replaceFirst(re!`\[b\]\[u\]# (.*)\[/u\]\[/b\]`, `$1`)
		.toFile(steam);
	remove(bb);
}

int dayFromScript(string script)
{
	switch (script.baseName())
	{
		case "anna.txt": return -1;
		case "pro1.txt": return -1;
		case "pro2.txt": return -1;
		case "pro3.txt": return -1;
		case "anna_script.txt": return 0;
		case "tutorial.txt": return 0;
		case "script1.txt": return 1;
		case "script2.txt": return 2;
		case "script3.txt": return 3;
		case "script4.txt": return 4;
		case "script5.txt": return 5;
		case "script6.txt": return 6;
		case "script7.txt": return 7;
		case "script8.txt": return 8;
		case "script9.txt": return 9;
		case "script10.txt": return 10;
		case "script11.txt": return 11;
		case "script12.txt": return 12;
		case "script13.txt": return 13;
		case "script14.txt": return 14;
		case "script15.txt": return 15;
		case "script16.txt": return 16;
		case "script17.txt": return 17;
		case "script18.txt": return 18;
		case "script19.txt": return 19;
		case "epilogues.txt": return 20;
		case "testroom.txt": return -1;
		case "gameover.txt": return -1;
		default: throw new Exception("Unknown script: " ~ script);
	}
}

void genSightings()
{
	auto sightings = loadSightings();
	auto f = File("15-section-sightings.steamguide", "wb");
	f.write(q"EOF
Variable index

You can use this table to reference when variables are set and accessed.
[list][*]A red circle (🔴) indicates that the variable is set on that day.
[*]A green circle (🟢) indicates that the variable is read on that day.
[*]An orange circle (🟠) indicates that the variable is both set and read on that day.[/list]

[table]
EOF");
	f.writeln("[tr][th]Variable[/th]", iota(0, 21).map!(n => "[th]%d[/th]".format(n)).join, "[/tr]");
	auto vars = sightings.byKey.map!(s => s.name.toString()).array.sort.uniq.array;
	foreach (var; vars)
	{
		if (var.among(
				  "bev_a",       "bev_b",
			"drinksize_a", "drinksize_b",
			   "flavor_a",    "flavor_b",
				 "kind_a",      "kind_b",
			  "alcohol_a",   "alcohol_b",
				  "otr_a",       "otr_b",

			"cur_client",
			"cur_day",
			"cur_stage",
			"jukebox_happens",
			"keeptext",
			"pickdare",
			"showdare",

			"drunklevel_a",
		))
			continue;

		if (!sightings.byKey.canFind!(s => s.name.toString() == var && dayFromScript(s.path) >= 0))
			continue;

		f.write("[tr][td]", var, "[/td]");
		foreach (day; 0 .. 21)
		{
			bool r = sightings.byKey.canFind!(s => s.name.toString() == var && dayFromScript(s.path) == day && !s.write);
			bool w = sightings.byKey.canFind!(s => s.name.toString() == var && dayFromScript(s.path) == day &&  s.write);
			f.write("[td]", r ? w ? "🟠" : "🟢" : w ? "🔴" : "", "[/td]");
		}
		f.writeln("[/tr]");
	}
	f.writeln("[/table]");
	f.close();
}

void main()
{
	foreach (de; dirEntries("", "*.steamguide", SpanMode.shallow))
		de.remove();
	string[string] prefaces;
	foreach (fn; dirEntries("", "*.org", SpanMode.shallow).map!(de => de.name).array.parallel)
	{
		auto steam = fn.setExtension(".steamguide");
		orgToSteam(fn, steam);
		if (steam.endsWith("-pre.steamguide"))
		{
			prefaces[steam.replace("-pre.steamguide", ".steamguide")] = readText(steam);
			remove(steam);
		}
	}

	genSightings();

	mkdirRecurse("images");
	foreach (de; dirEntries("images", "*.png", SpanMode.shallow))
		de.remove();
	copy("final-state.png", "images/final-state.png");
	foreach (i, line; readText("filelist.txt").splitLines.parallel(1))
	{
		if (!line.length) continue;
		auto parts = line.split("\t");
		enforce(parts.length == 4);

		auto id = parts[0];
		auto name = parts[1];
		auto title1 = parts[2];
		auto title2 = parts[3];

		stderr.writeln(name);

		auto fn = "10-section-%s.steamguide".format(id);
		auto section = File(fn, "w");
		section.writefln("%s - %s", title1, title2);
		section.writeln;
		if (fn in prefaces)
			section.writeln(prefaces[fn].strip ~ "\n");

		// Convert to PNG and SVG
		auto sectionPng = "../out/eng/" ~ name ~ ".dot.png";
		auto splitFiles = splitPNG(sectionPng, "images");

		foreach (png; splitFiles)
		{
			enforce(png.skipOver("images/"));
			section.writefln("[previewicon=%s;sizeOriginal][/previewicon]", png);
		}
	}
}
