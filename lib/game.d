module va11_hall_a.dialogue.lib.game;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.exception;
import std.format;
import std.range;

import ae.utils.array;

import gml_lsp_emu;

import va11_hall_a.dialogue.lib.constants;
import va11_hall_a.dialogue.lib.stateset;
import va11_hall_a.dialogue.lib.vars;

class VisitorGame : Game
{
	/*private*/ StateSetVisitor visitor;

	bool sealed;

	final StateSet save()
	{
		return visitor.currentSubset;
	}

	final void load(StateSet set)
	{
		visitor = StateSetVisitor(set);
	}

	final bool next() { return visitor.next(); }

	Value get(Addr addr)
	{
		return visitor.get(addr);
	}

	void put(Addr addr, Value value)
	{
		visitor.put(addr, value);
	}

	void inject(Addr addr, Value[] values)
	{
		visitor.inject(addr, values);
	}

	class VisitorObject : IObject
	{
		int id;
		VisitorGame game;
		this(int id, VisitorGame game) { this.id = id; this.game = game; }
		override int getID() { return id; }
		IObject dup() const pure { assert(false); }

		override Value get(IntStr name)
		{
			return game.get(Addr(id, name));
		}

		override void put(IntStr name, Value value)
		{
			game.put(Addr(id, name), value);
		}
	}

	class VisitorEnv : GameEnv
	{
		override int allocObject()
		{
			auto game = cast(VisitorGame)this.outer;
			if (game.sealed)
				assert(false, "Not allocating object after sealing");
			return super.allocObject();
		}

		override IObject newObject(int id)
		{
			auto game = cast(VisitorGame)this.outer;
			return new VisitorObject(id, game);
		}

		override IObject getObject(int id)
		{
			if (id < 0)
			{
				auto game = cast(VisitorGame)this.outer;
				return new VisitorObject(id, game);
			}
			return super.getObject(id);
		}

		override Value call(IntStr name, IObject self, Value[] arguments)
		{
			Value transform_add(string name)()
			{
				auto addr = .global!name;
				if (addr in willRead)
				{
					checkWrite(addr);

					enforce(arguments.length == 1);
					game.visitor.transform(addr, (ref Value v) { v = v + arguments[0]; });
				}
				return Value.void_;
			}

			void addAnnaChoice(short choice)
			{
				auto choices = game.visitor.get(Addrs.annaChoices);
				if (choices.type == Value.Type.undefined)
					choices = Value(Value.Type.array1);
				game.visitor.put(Addrs.annaChoices, Value(choices.array1 ~ Value(choice)));
			}

			// Builtins - TODO actually implement
			if (name == intstr!"ds_list_create")
				return Value(int16(0));
			else
			if (name == intstr!"ds_list_add")
				return Value.void_;
			else
			if (name == intstr!"ds_list_set")
				return Value.void_;
			else
			if (name == intstr!"ds_list_clear")
				return Value.void_;
			else
			// Builtins - overrides
			if (name == intstr!"choose")
			{
				auto game = cast(VisitorGame)this.outer;
				if (arguments.length && arguments.all!(argument => game.distractions.canFind(argument)))
					return Value("(random distracted order description - one of %(%d, %))".format(
						arguments.map!(argument => 1 + game.distractions.indexOf(argument))
					));
				else
				if (arguments == badDrinks)
					return Value("(random failed drink name)");
				else
					throw new Exception("Unknown choose call");
			}
			else
			if (name == intstr!"randomize")
				return Value.void_;
			else
			if (name == intstr!"instance_exists")
			{
				enforce(arguments.length == 1);
				switch (arguments[0].int16)
				{
					case ObjID.obj_textbox:
						return game.get(Addrs.textboxCreated);

					case ObjID.out_of_apartment:
						return Value(game.get(.global!"cur_day").to!int16.int16 > 18);

					default:
						return Value(false);
				}
			}
			else
			if (name == intstr!"room_goto")
			{
				enforce(arguments.length == 1);
				switch (arguments[0].int16)
				{
					case RoomID.week1_intro:
						game.visitor.put(Addrs.done, Done.sync);
						game.put(.global!"cur_stage", Value(int16(-2))); // gml_Object_intro1_alt_Create_0
						return Value.void_;

					case RoomID.jill_room:
					case RoomID.week2_intro:
					case RoomID.week3_intro:
						game.visitor.put(Addrs.done, Done.sync);
						return Value.void_;

					case RoomID.sunday2:
						game.textbox.put(intstr!"room", Value(RoomID.sunday2));

						self = new GObject;
						self.put(intstr!"x", Value(0.int16));
						self.put(intstr!"y", Value(0.int16));
						runFunction("gml_Object_sunday2_control_Create_0", self, []);

						self = new GObject;
						self.put(intstr!"room", Value(RoomID.sunday2));
						game.runFunction("gml_Object_alma_portrait_Create_0", self, []);
						game.runFunction("gml_Object_dana_portrait_Create_0", self, []);
						game.runFunction("gml_Object_doro_portrait_Create_0", self, []);
						game.runFunction("gml_Object_gil_portrait_Create_0" , self, []);
						game.runFunction("gml_Object_rad_portrait_Create_0" , self, []);

						return Value.void_;

					case RoomID.codec_room:
						game.textbox.put(intstr!"room", Value(RoomID.codec_room));

						self = new GObject();
						self.put(intstr!"x", Value(0.int16));
						self.put(intstr!"y", Value(0.int16));
						return runFunction("gml_Object_codec_control_Create_0", self, []);

					case RoomID.codecload2:
						return call(intstr!"room_goto", null, [Value(RoomID.codec_room)]);

					case RoomID.break_time: // Day 5->6 - Jill doesn't go home
						game.visitor.put(Addrs.done, Done.sync);
						return Value.void_;
					default:
						throw new Exception("Going to unknown room");
				}
			}
			else
			if (name == intstr!"instance_create")
			{
				enforce(arguments.length == 3);
				switch (arguments[2].int16)
				{
					case ObjID.shaker_a:
					case ObjID.dialog_control:

					case ObjID.save_home:
					case ObjID.break_bumper:
					case ObjID.break_savereturn:
					case ObjID.break_savehome:

					case ObjID.top_wordbanner:
					case ObjID.bottom_wordbanner:
					case ObjID.log_obj:
					case ObjID.jill_portrait:
					case ObjID.room_settings:
					case ObjID.jillroom_exit:
					case ObjID.text_control_codec:
					case ObjID.dana_portrait:
					case ObjID.moving_frame:
					case ObjID.dana_portrait_codec:
					case ObjID.alma_portrait_codec:
					case ObjID.sei_portrait:

						return Value.void_;

					case ObjID.cuttoblack_intro:
						return call(intstr!"room_goto", null, [Value(RoomID.week1_intro)]);

					case ObjID.out_of_apartment:
						self = new GObject;
						self.put(intstr!"alarm", Value([Value(0)]));
						runFunction("gml_Object_newday_text_Create_0", self, []);
						runFunction("gml_Object_newday_text_Alarm_0", null, []);
						if (game.get(.global!"cur_day").to!int16.int16 < 0)
							game.visitor.put(Addrs.done, Done.sync);
						return Value.void_;

					case ObjID.out_to_break:
						// TODO: cull variables not in save file
						runFunction("gml_Object_break_changer_Create_0", new GObject, []);
						if (game.visitor.get(Addrs.done) == Done.no)
							game.visitor.put(Addrs.done, Done.sync);
						return Value.void_;

					case ObjID.out_to_bar:
						game.textbox.put(intstr!"room", Value(RoomID.bar));
						if (game.get(.global!"cur_day").to!int16.int16 > 18)
							game.visitor.put(Addrs.done, Done.sync);
						return Value.void_;

					case ObjID.new_day:
					case ObjID.out_to_results:
						// TODO: cull variables not in save file
						self = new GObject();
						self.put(intstr!"step", Value(0));

						runFunction("gml_Object_new_day_Step_0", self, []);

						// gml_Object_show_room_Create_0
						if (game.get(.global!"cur_day").to!int16.int16 == 4)
							game.inject(.global!"porndl", [Value(1.int16), Value(2.int16)]);
						if (game.get(.global!"cur_day").to!int16.int16 == 12)
							game.inject(.global!"lightdl", [Value(1.int16), Value(2.int16)]);
						if (game.get(.global!"cur_day").to!int16.int16 == 18)
							game.inject(.global!"housedl", [Value(1.int16), Value(2.int16)]);

						if (game.get(.global!"cur_day").to!int16.int16 == 2)
						{
							// Buy (or don't) shop items.
							foreach (var; willRead.byKey)
								if (var.id == globalObj)
								{
									auto varName = var.name.toString();
									if (varName.startsWith("shop_") || varName.endsWith("song"))
										game.inject(var, [Value(0.int16), Value(1.int16)]);
								}
						}

						return Value.void_;

					case ObjID.out_to_end:
						return Value.void_;

					case ObjID.out_to_title:
					case ObjID.out_to_gameover:
					case ObjID.breakend_text:
					case ObjID.popup_room:
					case ObjID.anna_out:
						game.visitor.put(Addrs.done, Done.end);
						return Value.void_;

					case ObjID.credits_start:
						// gml_Object_credits_control_Create_0
						game.put(.global!"cur_end", Value(""));
						game.put(.global!"cur_stage", Value(int16(1)));
						game.put(.global!"ngplus_able", Value(int16(1)));
						game.put(.global!"ngplus_flag", Value(int16(1)));

						runFunction("gml_Object_ending_tester_Create_0", null, []);

						game.visitor.put(Addrs.done, Done.sync);
						return Value.void_;

					case ObjID.out_to_codec:
						game.visitor.put(Addrs.done, Done.sync);
						return call(intstr!"room_goto", null, [Value(RoomID.codec_room)]);

					case ObjID.out_to_balcony:
						game.put(Addrs.roomGoto, Value(RoomID.sunday1));

						auto cur_beer = Addr(game.sunday1_show.getID(), intstr!"cur_beer");
						game.inject(cur_beer, iota(short(0), short(12 + 1)).map!(n => Value(n)).array);

						game.visitor.put(Addrs.textboxCreated, Value(true));
						game.visitor.put(Addrs.done, Done.sync);
						return Value.void_;

					case ObjID.intro1_text:
						// TODO: add effect: show intro text?

						// gml_Object_intro1_text_Step_0
						game.put(.global!"cur_day", Value(int16(1)));
						game.put(.global!"cur_stage", Value(int16(0)));
						// game.textbox.put(intstr!"room", Value(RoomID.jill_room));
						goto case ObjID.out_of_apartment;

					case ObjID.out_to_dareload:
						self = new GObject();
						self.put(intstr!"step", Value(0));
						return runFunction("gml_Object_out_to_dare_Step_0", self, []);

					case ObjID.out_to_codecload2:
						self = new GObject();
						self.put(intstr!"step", Value(0));
						return runFunction("gml_Object_out_to_codecload2_Step_0", self, []);

					case ObjID.annachoice_1: addAnnaChoice(1); return Value.void_;
					case ObjID.annachoice_2: addAnnaChoice(2); return Value.void_;
					case ObjID.annachoice_3: addAnnaChoice(3); return Value.void_;
					case ObjID.annachoice_4: addAnnaChoice(4); return Value.void_;
					case ObjID.annachoice_5: addAnnaChoice(5); return Value.void_;
					case ObjID.annachoice_6: addAnnaChoice(6); return Value.void_;
					case ObjID.annachoice_7: addAnnaChoice(7); return Value.void_;
					case ObjID.annachoice_8: addAnnaChoice(8); return Value.void_;
					case ObjID.annachoice_9: addAnnaChoice(9); return Value.void_;

					default:
						throw new Exception("Unknown object created");
				}
			}
			else
			if (name == intstr!"draw_set_font")
				return Value.init;
			else
			if (name == intstr!"string_height")
				return Value(10);
			else
			if (name == intstr!"string_width")
				return Value(100);
			else
			if (name == intstr!"surface_create")
				return Value.init;
			else
			if (name == intstr!"action_move_start")
				return Value.init;
			else
			if (name == intstr!"action_reverse_ydir")
				return Value.init;
			else
			if (name == intstr!"steam_get_achievement")
			{
				auto v = game.visitor.get(Addr(steamAchievementsObj, IntStr(arguments[0].string)));
				if (v == Value.undefined)
					v = Value(false);
				return v;
			}
			else
			if (name == intstr!"steam_set_achievement")
			{
				enforce(arguments.length == 1);
				game.visitor.put(Addr(steamAchievementsObj, IntStr(arguments[0].string)), Value(true));
				game.visitor.put(Addrs.steamAchievements, Value(game.visitor.get(Addrs.steamAchievements).array1 ~ arguments[0]));
				return Value.void_;
			}
			else
			if (name == intstr!"place_meeting") // For gml_Object_annachoice_*_Step_0
				return Value(true);
			else
			if (name == intstr!"mouse_check_button_pressed") // For gml_Object_annachoice_*_Step_0
				return Value(true);
			else
			// Game function overrides
			if (name.among(intstr!"config_init", intstr!"update_loads", intstr!"update_saves_start", intstr!"annaconfig_save"))
				return Value.void_;
			else
			if (name == intstr!"mixertips")
			{
				enforce(arguments.length == 4);
				game.visitor.put(Addrs.mixertips, Value(game.visitor.get(Addrs.mixertips).array1 ~ Value(arguments)));
				return super.call(name, self, arguments);
			}
			else
			if (name == intstr!"mixertips_double")
			{
				enforce(arguments.length == 6);
				game.visitor.put(Addrs.mixertips, Value(game.visitor.get(Addrs.mixertips).array1 ~ Value(arguments)));
				return super.call(name, self, arguments);
			}
			else
			if (name == intstr!"drinkqueue_add")
			{
				return Value.void_;
			}
			else
			if (name.among(intstr!"textbox_create", intstr!"textbox_create_alt"))
			{
				enforce(arguments.length == 3);
				enforce(arguments[2].int16 == 1); // textbox_skip_possible
				game.visitor.put(Addrs.scriptPath, arguments[0]);
				game.visitor.put(Addrs.scriptE, arguments[1]);
				game.visitor.put(Addrs.textboxCreated, Value(true));
				return Value.void_;
			}
			else
			// Hooks
			if (name == intstr!"__noop")
				return Value.void_;
			else
			if (name == intstr!"__add_drunklevel_a")
				return transform_add!"drunklevel_a"();
			else
			if (name == intstr!"__add_tipcounter")
				return transform_add!"tipcounter"();
			else
			if (name == intstr!"__add_cashcounter")
				return transform_add!"cashcounter"();
			else
			if (name == intstr!"__add_barscore")
				return transform_add!"barscore"();
			else
			if (name == intstr!"__add_mistakecounter")
				return transform_add!"mistakecounter"();
			else
				return super.call(name, self, arguments);
		}
	}

	override string filterProgram(string name, string s)
	{
		s = s
			.replace(`call (audio_play_sound[]:int32 `, `call (__noop[]:int32 `)
			.replace(`call (audio_sound_gain[]:int32 `, `call (__noop[]:int32 `)
			.replace(`call (audio_stop_all[]:int32 `  , `call (__noop[]:int32 `)
			.replace(`(instance_create[]:int32 self.x self.y `, `(instance_create[]:int32 0s 0s `)
			.replace(`global.drunklevel_a = (+ global.drunklevel_a `,     `call (__add_drunklevel_a[]:int32 `)
			.replace(`global.tipcounter = (+ global.tipcounter `,         `call (__add_tipcounter[]:int32 `)
			.replace(`global.cashcounter = (+ global.cashcounter `,       `call (__add_cashcounter[]:int32 `)
			.replace(`global.barscore = (+ global.barscore `,             `call (__add_barscore[]:int32 `)
			.replace(`global.mistakecounter = (+ global.mistakecounter `, `call (__add_mistakecounter[]:int32 `)
		;

		return s;
	}

	Value[] distractions;
	IObject textbox, sunday1_show;

	enum gameDir = "decompiled";

	this()
	{
		env = new VisitorEnv;
		super(gameDir);
	}

	static Value[] badDrinks;
	static this()
	{
		badDrinks = ["\\#\\#20%!!", "n\\#@\\#\\#*", "f@\\#\\#\\#", "\\#y\\#bb=", "\\#u??!!", "gt??!\\#"].amap!(s => Value(s)).dup;
	}
}

final class GraphGame : VisitorGame
{
	Value[Addr] constants;

	override Value get(Addr addr)
	{
		if (addr.id < 0)
			return super.get(addr);
		checkRead(addr);
		if (addr !in willWrite)
			return constants.get(addr, Value.undefined);
		else
			return super.get(addr);
	}

	override void put(Addr addr, Value value)
	{
		if (addr.id < 0)
			return super.put(addr, value);

		if (!game.sealed)
		{
			if (addr !in willRead)
				{}
			else
			if (addr !in willWrite)
				constants[addr] = value;
			else
				super.put(addr, value);
			return;
		}

		if (addr !in willRead)
			return;
		checkWrite(addr);

		super.put(addr, value);
		if (game.sealed && addr.id == env.getGlobal().getID() && !addr.name.among(
			intstr!"mixing",
			intstr!"mixhappens",
			intstr!"change_slot",
			intstr!"slotamount",
			intstr!"otr_a"        , intstr!"otr_b"        ,
			intstr!"age_a"        , intstr!"age_b"        ,
			intstr!"mod_slot_a"   , intstr!"mod_slot_b"   ,
			intstr!"mod_aa"       , intstr!"mod_ab"       ,
			intstr!"mod_ba"       , intstr!"mod_bb"       ,
			intstr!"mod_ca"       , intstr!"mod_cb"       ,
			intstr!"mod_da"       , intstr!"mod_db"       ,
			intstr!"mod_ea"       , intstr!"mod_eb"       ,
			intstr!"mixed_a"      , intstr!"mixed_b"      ,
			intstr!"failed_a"     , intstr!"failed_b"     ,
			intstr!"mix_a"        , intstr!"mix_b"        ,
			intstr!"bev_a"        , intstr!"bev_b"        ,
			intstr!"bevid_a"      , intstr!"bevid_b"      ,
			intstr!"description_a", intstr!"description_b",
			intstr!"drinktype_a"  , intstr!"drinktype_b"  ,
			intstr!"drinkscore_a" , intstr!"drinkscore_b" ,
			intstr!"flavor_a"     , intstr!"flavor_b"     ,
			intstr!"kind_a"       , intstr!"kind_b"       ,
			intstr!"drinksize_a"  , intstr!"drinksize_b"  ,
			intstr!"alcohol_a"    , intstr!"alcohol_b"    ,
			intstr!"exdrink_a"    , intstr!"exdrink_b"    ,

			intstr!"orders",
			intstr!"ft_any",
			intstr!"barscore",
			intstr!"pickdare",
			intstr!"showdare",

			intstr!"shouldpay",
			intstr!"rightdrink", intstr!"rightdrink1", intstr!"rightdrink2",
			intstr!"big_able"  , intstr!"big_able1"  , intstr!"big_able2"  ,
			intstr!"tipping",
		))
			visitor.put(Addr(newGlobalsObj, addr.name), value);
	}

	override void inject(Addr addr, Value[] values)
	{
		checkWrite(addr);
		super.inject(addr, values);
	}
}

GraphGame game;
StateSet initialState;

void initGame()
{
	game = new GraphGame();
	game.load(StateSet.unitSet);
	game.visitor.next();

	assert(game.env.getGlobal().getID() == globalObj, "Global object ID mismatch");

	game.env.getGlobal().put(intstr!"language", Value("en"));
	game.env.getGlobal().put(intstr!"fnt_textbox", Value.void_);
	game.env.getObject(steamAchievementsObj).put(intstr!"CREVICE_TROPHY", Value(true)); // optimization
	game.env.getObject(steamAchievementsObj).put(intstr!"RIGHT_WRONG_TROPHY", Value(true)); // optimization

	game.runFunction("gml_Object_strings_controller_Create_0", null, []);
	game.runFunction("gml_Object_var_controller_Create_0", new GObject, []);
	game.runFunction("gml_Script_freshnew_reset", null, []);

	game.textbox = game.env.getObject(game.env.allocObject());
	game.textbox.put(intstr!"view_hport", Value([Value(0)]));
	game.textbox.put(intstr!"view_wport", Value([Value(0)]));
	game.textbox.put(intstr!"room", Value(RoomID.bar));

	game.runFunction("gml_Object_obj_textbox_Create_0", game.textbox, []);

	game.sunday1_show = game.env.getObject(game.env.allocObject());
	game.env.addGlobalObject(intstr!"sunday1_show", game.sunday1_show.getID());

	auto drinkSpriteID = game.env.allocObject();
	game.env.addGlobalObject(intstr!"drinksprite_a", drinkSpriteID);
	game.env.addGlobalObject(intstr!"drinksprite_b", drinkSpriteID);

	game.env.getObject(privateObj).put(Addrs.done.name, Done.no);

	game.distractions = iota(1, 30 + 1)
		.map!(n => n.format!"distract%d".IntStr)
		.map!(v => game.env.getGlobal().get(v))
		.array;

	game.sealed = true;
	initialState = game.save();
	assert(initialState.count == 1);

	foreach (addr; [
		// TODO: Access these via GraphGame to avoid having to hard-code them here
		global!"mixhappens",
		global!"jukebox_happens",
		global!"slotamount",
		global!"cur_day"   ,
		global!"cur_stage" ,
		global!"cur_client",
	])
	{
		checkWrite(addr);
		checkRead (addr);
	}
}
