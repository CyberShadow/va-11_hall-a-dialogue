module va11_hall_a.dialogue.lib.serialization;

import std.algorithm.iteration;
import std.array;
import std.stdio;
import std.traits;
import std.typecons;

import ae.utils.aa;

import gml_lsp_emu.intstr;
import gml_lsp_emu.program;

import va11_hall_a.dialogue.lib.stateset;
import va11_hall_a.dialogue.lib.vars;

struct Serializer(Stream)
{
	static if (is(Stream == typeof(File.init.lockingBinaryWriter())))
		enum serializing = true;  // reading from memory, writing to file
	else
	static if (is(Stream == File))
		enum serializing = false; // reading from file, writing to memory
	else
		static assert(false);

	Stream* stream;

	IntStr[IntStr.Index] visitedStrings;
	immutable(StateSet.Node)*[size_t] visitedNodes;
	void*[size_t] visitedPointers;

	private void convert(T)(lazy T serializer, void delegate(ref T) deserializer)
	{
		static if (serializing)
		{
			T t = serializer();
			visit(t); // write to file
		}
		else
		{
			T t;
			visit(t); // read from file
			deserializer(t);
		}
	}

	private void visitRef(
		bool checkNull,
		ID,
		P,
		C,
		QP, // Qualified pointer
	)(
		ref QP value,
		ref P[ID] visited,
		lazy ID getID,
		void delegate() serializeContents,
		P delegate(ref C) contentsToPointer,
	)
	{
		static if (serializing)
		{
			ID id = getID;
			visit(id);
			visited.updateVoid(id,
				{
					if (!checkNull || id !is ID.init)
						serializeContents();
					return value;
				},
				(ref P pointer)
				{
					// already serialized
				},
			);
		}
		else
		{
			ID id;
			visit(id);
			visited.updateVoid(id,
				{
					static if (checkNull)
						if (id is ID.init)
							return P.init;
					C contents;
					visit(contents);
					P pointer = contentsToPointer(contents);
					value = pointer;
					return pointer;
				},
				(ref P pointer)
				{
					value = pointer;
				},
			);
		}
	}

	// size_t offset;

	void visit(T)(ref T value)
	{
		// writefln("%s %s %s", "<>"[serializing], offset, T.stringof);
		static if (is(Unqual!T == IntStr))
		{
			// convert(value.toString(), (ref string v) { static if (serializing) assert(false); else value = IntStr(v); });
			visitRef!(
				false,
				IntStr.Index,
				IntStr,
				string,
			)(
				value,
				visitedStrings,
				value.index,
				{ static if (serializing) { string s = value.toString(); visit(s); } else assert(false); },
				(ref string s) => IntStr(s),
			);
		}
		else
		static if (is(Unqual!T == Value))
		{
			visit(value.type);
			foreach (Type; Value.types)
				static if (!is(Unqual!Type == void))
					if (value.type == Value.typeOf!Type)
						visit(value.get!Type());
		}
		else
		static if (is(T U : U*))
		{
			// convert!(() => cast(size_t)value.root, (ref v) { value = sets[v]; })();
			static if (is(U == immutable(StateSet.Node)))
				visitRef!(
					false,
					size_t,
					immutable(StateSet.Node)*,
					StateSet.Node,
				)(
					value,
					visitedNodes,
					cast(size_t) value,
					{ static if (serializing) visit(*value); else assert(false); },
					(ref StateSet.Node node) => cast(immutable) [node].ptr,
				);
			else
				visitRef!(
					true,
					size_t,
					void*,
					U,
					void*,
				)(
					*cast(void**)&value,
					visitedPointers,
					cast(size_t) value,
					{ static if (serializing) visit(*value); else assert(false); },
					(ref U v) => [v].ptr,
				);
		}
		else
		static if (is(T U : U[]))
		{
			convert(value.length, (ref size_t v) { static if (serializing) assert(false); else value.length = v; });
			foreach (ref e; cast(Unqual!U[]) value)
				visit(e);
		}
		else
		static if (is(T V : V[K], K))
		{
			static struct Pair { K key; V value; }
			// https://issues.dlang.org/show_bug.cgi?id=21272
			Pair[] pairs(V[K] aa) { Pair[] result; foreach (key, value_; aa) result ~= Pair(key, value_); return result; }
			V[K] fromPairs(Pair[] pairs) { V[K] result; foreach (ref pair; pairs) result[pair.key] = pair.value; return result; }
			convert(
				// https://issues.dlang.org/show_bug.cgi?id=21245
				// value.byPair.array,
				pairs(value),
				(ref Pair[] v) { static if (serializing) assert(false); else value = fromPairs(v); }
			);
		}
		else
		static if (__traits(hasMember, T, "visit"))
		{
			static struct Handler
			{
				Serializer* self;
				T* value;

				bool handle(F)(scope F* delegate(T*) dg)
				{
					auto a = dg(value);
					self.visit(*a);
					return false;
				}
			}

			Handler handler;
			handler.self = &this;
			handler.value = &value;
			value.visit(&handler);
		}
		else
		static if (is(T == struct))
		{
			foreach (ref f; value.tupleof)
				visit(f);
			static if (!serializing && is(Unqual!T == StateSet))
				value = value.deduplicate();
		}
		else
		static if (is(T : real))
		{
			static if (serializing)
				stream.rawWrite((&value)[0 .. 1]);
			else
				stream.rawRead((&value)[0 .. 1]);
			// offset += T.sizeof;
		}
		else
		static if (is(Unqual!T : IObject))
			assert(false);
		else
			static assert(false, "Don't know how to visit " ~ T.stringof);
	}
}

void serialize(Stream, T)(ref Stream stream, ref T value)
{
	Serializer!Stream s;
	s.stream = &stream;
	foreach (set; [StateSet.emptySet, StateSet.unitSet])
		s.visitedNodes[cast(size_t) set.root] = set.root;
	s.visit(value);
}

void save(T)(auto ref T value, File f)
{
	auto writer = f.lockingBinaryWriter();
	serialize(writer, value);
}

T load(T)(File f)
{
	T value;
	serialize(f, value);
	return value;
}

unittest
{
	void test(T)(T value)
	{
		auto f = File.tmpfile();
		save(value, f);
		f.seek(0);
		auto value2 = load!(Unqual!T)(f);
		if (value != value2)
		{
			f.seek(0);
			ubyte[] bytes = new ubyte[f.size];
			f.rawRead(bytes);
			writeln("Wrote: ", value);
			writeln("Read : ", value2);
			writefln("Bytes: [%(%02X %)]", bytes);
			assert(false);
		}
	}

	test(StateSet.emptySet);
	test(StateSet.unitSet);
	test(StateSet.unitSet.cartesianProduct(Addr(1, IntStr("abc")), [Value(1), Value(2), Value(3)]));

	static struct IntPtr
	{
		int* ptr;
		bool opEquals(ref const IntPtr other) const { return ptr == other.ptr || *ptr == *other.ptr; }
	}

	IntPtr[int] aa;
	aa[1] = IntPtr(new int(2));
	aa[2] = IntPtr.init;
	test(aa);
}
