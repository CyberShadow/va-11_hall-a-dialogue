module va11_hall_a.dialogue.lib.stateset;

import std.algorithm.searching;
import std.algorithm.sorting;
import std.bigint;
import std.format : formattedWrite;
import std.range;
import std.string;
import std.traits;

import ae.utils.array;
import ae.utils.mapset;

import gml_lsp_emu.intstr;
import gml_lsp_emu.program : Value;

import va11_hall_a.dialogue.lib.vars;

alias StateSet = MapSet!(Addr, Value);
alias StateSetVisitor = MapSetVisitor!(Addr, Value);

import std.stdio : File, stdout;
void dumpStateSet(StateSet set, File f = stdout)
{
	HashSet!StateSet dumped;
	void dump(StateSet set, int indent = 0)
	{
		if (set is StateSet.emptySet)
			f.writefln("%*s{}", indent, "");
		else
		if (set is StateSet.unitSet)
			f.writefln("%*s{[]}", indent, "");
		else
		{
			if (set in dumped)
				f.writefln("%*s@%X", indent, "", cast(size_t)set.root);
			else
			{
				dumped.add(set);
				f.writefln("%*s@%X %s:", indent, "", cast(size_t)set.root, set.root.dim);
				foreach (pair; set.root.children)
				{
					f.writefln("%*s- %s", indent, "", pair.value);
					dump(pair.set, indent + 2);
				}
			}
		}
	}
	dump(set);
}

void dumpStateSetGraph(StateSet set, File f = stdout)
{
	import std.conv : text;
	f.writeln("digraph {");
	HashSet!StateSet visited;
	void visit(StateSet set)
	{
		if (set in visited)
			return;
		visited.add(set);
		f.writefln("\tS%s [label=%(%s%)]", set.root, [set is StateSet.emptySet ? "{}" : set is StateSet.unitSet ? "{[]}" : text(set.root.dim)]);
		if (set is StateSet.emptySet ||  set is StateSet.unitSet)
			return;
		foreach (ref pair; set.root.children)
		{
			f.writefln("\tS%s -> S%s [label=%(%s%)]", set.root, pair.set.root, [text(pair.value)]);
			visit(pair.set);
		}
	}
	visit(set);
	f.writeln("}");
}

struct ExploredPoint
{
	string scriptPath;
	int scriptE;
	StateSet expanded;
}

/// Like `MapSet.all`, but with a predicate
void withAllValuesOf(DimName, DimValue)(MapSet!(DimName, DimValue) set, DimName dim, scope void delegate(DimValue value) fun)
{
	alias Set = typeof(set);
	enum nullValue = DimValue.init;

	if (set is Set.emptySet) return;

	HashSet!Set seen;
	void scan(Set set)
	{
		if (set is Set.unitSet)
			return fun(nullValue);
		if (set in seen)
			return;
		seen.add(set);

		if (set.root.dim == dim)
		{
			foreach (ref pair; set.root.children)
				fun(pair.value);
			return;
		}

		foreach (ref pair; set.root.children)
			scan(pair.set);
	}
	scan(set);
}

/// Is there a non-empty subset with the given dimension at the given value?
/// Like `set.all(dim).indexOf(value) >= 0`
bool hasValue(DimName, DimValue)(MapSet!(DimName, DimValue) set, DimName dim, DimValue value)
{
	alias Set = typeof(set);
	enum nullValue = DimValue.init;

	if (set is Set.emptySet) return false;

	HashSet!Set seen;
	bool scan(Set set)
	{
		if (set is Set.unitSet)
			return value == nullValue;
		if (set in seen)
			return false;
		seen.add(set);

		if (set.root.dim == dim)
		{
			foreach (ref pair; set.root.children)
				if (pair.value == value)
					return true;
			return false;
		}

		foreach (ref pair; set.root.children)
			if (scan(pair.set))
				return true;

		return false;
	}
	return scan(set);
}

/// Like `set.get(givenDim, givenValue).all(soughtDim)`, but faster.
HashSet!DimValue getAllFor(DimName, DimValue)(MapSet!(DimName, DimValue) set, DimName givenDim, DimValue givenValue, DimName soughtDim)
{
	alias Set = typeof(set);
	enum nullValue = DimValue.init;

	if (set is Set.emptySet)
		return HashSet!DimValue.init;

	HashSet!DimValue soughtValues;
	HashSet!Set seen;

	void scanNeither(Set set)
	{
		if (set is Set.unitSet)
		{
			if (givenValue == nullValue)
				soughtValues.add(nullValue);
			return;
		}
		if (set in seen)
			return;
		seen.add(set);

		if (set.root.dim == givenDim)
		{
			foreach (ref pair; set.root.children)
				if (pair.value == givenValue)
					pair.set.withAllValuesOf(soughtDim, (DimValue soughtValue) { soughtValues.add(soughtValue); });
			return;
		}

		if (set.root.dim == soughtDim)
		{
			foreach (ref pair; set.root.children)
				if (pair.set.hasValue(givenDim, givenValue))
					soughtValues.add(pair.value);
			return;
		}

		foreach (ref pair; set.root.children)
			scanNeither(pair.set);
	}
	scanNeither(set);

	return soughtValues;
}

unittest
{
	StateSet.unitSet.getAllFor(Addr.init, Value.init, Addr.init);
}

MapSet!(DimName, DimValue) copyDim(DimName, DimValue)(MapSet!(DimName, DimValue) set, DimName from, DimName to)
{
	alias Set = typeof(set);

	if (set is Set.emptySet)
		return set;

	Set[Set] cache;
	Set scan(Set set)
	{
		if (set == Set.unitSet)
			//throw new Exception("Dim not found");
			return set;
		return cache.require(set, {
			if (set.root.dim == from)
			{
				auto newChildren = set.root.children.dup;
				foreach (i, ref pair; newChildren)
					pair.set = Set(new immutable Set.Node(to, set.root.children[i .. i + 1])).deduplicate;
				return Set(new immutable Set.Node(from, cast(immutable) newChildren)).deduplicate;
			}
			return set.lazyMap(&scan);
		}());
	}
	return scan(set);
}

unittest
{
	alias M = MapSet!(string, int);
	auto m = M.emptySet
		.merge(M.unitSet.set("x", 1).set("y", 2))
		.merge(M.unitSet.set("x", 2).set("y", 4))
		.merge(M.unitSet.set("x", 3).set("y", 6));
	assert(m.copyDim("x", "x2").get("x2", 2).all("y") == [4]);
	assert(m.copyDim("y", "y2").get("y2", 4).all("x") == [2]);
	assert(m.copyDim("z", "z2") is m);
}

MapSet!(DimName, DimValue) optimize2(DimName, DimValue)(MapSet!(DimName, DimValue) set)
{
	alias Set = typeof(set);
	// enum nullValue = DimValue.init;

	set = set.optimize();
	auto dims = set.getDims();
	if (dims.length < 2)
		return set;

	auto nodes = set.uniqueNodes();

	foreach (depth0; 0 .. dims.length - 1)
	{
		foreach (depth1; depth0 .. dims.length - 1)
		{
			auto set2 = set;
			foreach_reverse (depth2; depth0 .. depth1)
				set2 = set2.swapDepth(depth2);
			auto nodes2 = set2.uniqueNodes;
			if (nodes2 < nodes)
			{
				nodes = nodes2;
				set = set2;
			}
		}
	}

	return set;
}

HashSet!(MapSet!(DimName, DimValue)) nodesAtDepth(DimName, DimValue)(HashSet!(MapSet!(DimName, DimValue)) sets, size_t depth)
{
	if (depth == 0)
		return sets;
	HashSet!(MapSet!(DimName, DimValue)) next;
	foreach (set; sets)
		foreach (ref child; set.root.children)
			next.add(child.set);
	return next.nodesAtDepth!(DimName, DimValue)(depth - 1);
}

MapSet!(DimName, DimValue) cutUntilDepth(DimName, DimValue)(MapSet!(DimName, DimValue) set, size_t depth)
{
	alias Set = typeof(set);
	if (depth == 0)
		return Set.unitSet;

	return set.lazyMap(subset => subset.cutUntilDepth(depth - 1));
}

MapSet!(DimName, DimValue) optimize3(DimName, DimValue)(MapSet!(DimName, DimValue) set, size_t depthLimit = 3)
{
	alias Set = typeof(set);
	// enum nullValue = DimValue.init;

	Set[] segments;

	set = set.optimize();
	auto dims = set.getDims();
	if (dims.length < 2)
		return set;

	// auto nodes = set.uniqueNodes();

	size_t depth0 = 0;
loop0:
	while (set != Set.unitSet)
	{
		bool search(size_t depth0, size_t depthMax, ref Set set)
		{
			if (depth0 == depthMax)
				return set.only.toSet.nodesAtDepth!(DimName, DimValue)(depth0).length == 1;

			foreach (depth1; depth0 .. dims.length - 1)
			{
				auto set2 = set;
				foreach_reverse (depth2; depth0 .. depth1)
					set2 = set2.swapDepth(depth2);
				if (search(depth0 + 1, depthMax, set2))
				{
					set = set2;
					return true;
				}
			}

			return false;
		}

		foreach (depthMax; 1 .. dims.length - 1 - depth0)
		{
			if (search(0, depthMax, set))
			{
				segments ~= set.cutUntilDepth(depthMax);
				auto result = set.only.toSet.nodesAtDepth!(DimName, DimValue)(depthMax);
				assert(result.length == 1);
				set = result.byKey.front;
				depth0 += depthMax;
				continue loop0;
			}
			if (depthMax == depthLimit)
			{
				set = set.lazyMap(subset => subset.optimize3(depthLimit));
				break;
			}
		}
		break;
	}

	foreach (segment; segments)
		set = set.cartesianProduct(segment);
	return set;
}

unittest
{
	alias M = MapSet!(string, int);
	M m;
	m = m.optimize2();
	m = m.optimize3();
}

BigInt bigCount(DimName, DimValue)(MapSet!(DimName, DimValue) set)
{
	alias Set = typeof(set);

	if (set is Set.emptySet)
		return BigInt(0);

	BigInt[Set] cache;
	BigInt scan(Set set)
	{
		if (set == Set.unitSet)
			return BigInt(1);
		return cache.require(set, {
			auto result = BigInt(0);
			foreach (ref pair; set.root.children)
				result += scan(pair.set);
			return result;
		}());
	}
	return scan(set);
}

unittest
{
	alias M = MapSet!(int, int);
	M m = M.unitSet;
	auto values = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
	foreach (d; 0 .. 100)
		m = m.cartesianProduct(d, values);
	assert(m.bigCount == BigInt(values.length) ^^ 100);
}

// Get subset containing these values.
MapSet!(DimName, DimValue) get(DimName, DimValue)(MapSet!(DimName, DimValue) set, DimValue[] values)
{
	alias Set = typeof(set);
	auto result = Set.emptySet;
	foreach (value; values)
		result = result.merge(set.get(value));
	return result;
}

MapSet!(DimName, DimValue) map(alias pred, DimName, DimValue)(MapSet!(DimName, DimValue) set)
{
	alias Set = typeof(set);
	auto visitor = MapSetVisitor!(DimName, DimValue)(set);
	Set result = Set.emptySet;
	while (visitor.next())
	{
		pred(&visitor);
		result = result.merge(visitor.currentSubset);
	}
	return result;
}

MapSet!(DimName, DimValue) filter(alias pred, DimName, DimValue)(MapSet!(DimName, DimValue) set)
{
	alias Set = typeof(set);
	auto visitor = MapSetVisitor!(DimName, DimValue)(set);
	Set result = Set.emptySet;
	while (visitor.next())
		if (pred(&visitor))
			result = result.merge(visitor.currentSubset);
	return result;
}

unittest
{
	alias M = MapSet!(string, int);
	M m = M.unitSet
		.cartesianProduct("x", [1, 2, 3])
		.cartesianProduct("y", [1, 2, 3])
		.filter!(v => (v.get("x") * v.get("y")) % 2 == 1)
	;
	assert(m.all("x").dup.sort.release == [1, 3]);
}
