module va11_hall_a.dialogue.lib.expansion;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.format;
import std.range;
import std.stdio;

import ae.utils.array;
import ae.utils.json;
import ae.utils.math;
import ae.utils.text.ascii : toDec;

import gml_lsp_emu;

import va11_hall_a.dialogue.lib.constants;
import va11_hall_a.dialogue.lib.game;
import va11_hall_a.dialogue.lib.stateset;
import va11_hall_a.dialogue.lib.script;
import va11_hall_a.dialogue.lib.serialization;
import va11_hall_a.dialogue.lib.vars;

void playScript()
{
	// stderr.writefln("Playing %s : %d", state.pos.path, state.pos.e);

	auto scriptPath = game.env.getObject(privateObj).get(Addrs.scriptPath.name).string;
	auto scriptE    = game.env.getObject(privateObj).get(Addrs.scriptE.name).to!int32.int32;

	auto script = getDayScript(scriptPath);
	auto segment = script.get(scriptE, Segment.init);
	enforce(segment !is Segment.init, format("No label %d in %s", scriptE, scriptPath));

	foreach (token; segment.tokens)
		if (token.type == Token.Type.tag)
		{
			auto tag = token.parseTag;
			switch (tag.tag)
			{
				case "E":
				case "C":
				case "STOPLIP":
				case "SHOW":
				case "SHOWF":

				case "CRASH":
				case "BOOM":
				case "SHAKE":
				case "SILENCE":
				case "CHAT":

				case "ANNA":
				case "CHANNEL":
				case "CHANGE":
				case "FRAME":

				case "ECHO": // TODO - some anna crypt stuff
					break;
				// case "XS":
				// case "RES":
				// case "ODR1":
				// case "ODR2":
				// case "ODR3":
				// case "ODR4":
				// case "ODR5":
				default:
				{
					if (token.text.among("XS:ph,1}[E:7", "XS:normatalk,1[C:33"))
						break; // Script bug
					scope(failure) stderr.writefln("Error while evaluating tag %s:", token);
					game.env.call(intstr!"textbox_cmd_execute", game.textbox, [Value(token.text)]);
					break;
				}
					// throw new Exception("Unknown tag: " ~ text(token));
			}
		}
}

bool scriptHasRes()
{
	auto scriptPath = game.env.getObject(privateObj).get(Addrs.scriptPath.name).string;
	auto scriptE    = game.env.getObject(privateObj).get(Addrs.scriptE.name).to!int32.int32;
	auto script = getDayScript(scriptPath);
	auto segment = script.get(scriptE, Segment.init);
	enforce(segment.tokens[0].type == Token.Type.tag && segment.tokens[0].parseTag.tag == "E");
	foreach (ref token; segment.tokens[1..$])
		if (token.type == Token.Type.tag)
		{
			auto tag = token.parseTag();
			switch (tag.tag)
			{
				case "STOPLIP":
				case "V":
					break;
				case "RES":
					return true;
				default:
					writeln(token);
					return false;
			}
		}
	return false;
}

StateSet drinksA, drinksB;

void loadDrinks()
{
	drinksA = load!StateSet(File("drinks-a.set", "rb"));
	drinksB = load!StateSet(File("drinks-b.set", "rb"));
	liveSets[drinksA]++;
	liveSets[drinksB]++;
}

StateSet applyDrinks(StateSet subset, StateSet drinks, ref StateSetVisitor visitor)
{
	static immutable vars = ["cur_day", "cur_stage", "cur_client"];
	static foreach (var; vars)
		drinks = drinks.slice(global!var, visitor.get(global!var).to!int16);
	if (drinks is StateSet.emptySet)
	{
		static foreach (var; vars)
			checkVarValue(intstr!var, visitor.get(global!var).to!int16);
		throw new Exception("Unknown day/stage/client");
	}

	// Some variables are set conditionally.
	// Take care to only overwrite them when they would be overwritten.

	auto dimsAndValues = drinks.getDimsAndValues();
	Addr[] partialVars;
	foreach (dim, values; dimsAndValues)
		if (Value.undefined in values)
			partialVars ~= dim;

	StateSet result;
	auto partialsVisitor = StateSetVisitor(drinks);
	auto partialValues = new Value[partialVars.length];
	while (partialsVisitor.next())
	{
		foreach (i, var; partialVars)
			partialValues[i] = partialsVisitor.get(var);
		auto remainder = partialsVisitor.currentSubset;
		foreach (i, var; partialVars)
		{
			auto value = partialValues[i];
			if (value.type == Value.Type.undefined)
				remainder = remainder.remove(var);
			else
			if (!var.among(global!"ft_any"))
				remainder = remainder.set(Addr(newGlobalsObj, var.name), value);
		}
		result = result.merge(subset.cartesianProduct(remainder));
	}

	return result;
}

bool advance()
{
	// Clear effects
	game.put(Addrs.mixertips        , Value((Value[]).init));
	game.put(Addrs.done             , Done.no);
	game.put(Addrs.orderDesc        , Value.undefined);
	game.put(Addrs.steamAchievements, Value((Value[]).init));

	switch (game.env.getObject(privateObj).get(Addrs.runMode.name).int32)
	{
		case RunMode.code.int32:
		{
			game.put(Addrs.textboxCreated, Value(false));

			auto origRoom = game.textbox.get(intstr!"room");

			StateSet lastIter;
			while (true)
			{
				if (game.get(Addrs.gameOver).type != Value.Type.undefined)
				{
					game.put(Addrs.gameOver, Value.undefined);
					game.env.call(intstr!"gameover_mix", null, []);
				}
				else
				if (game.get(global!"jukebox_happens").to!bool.boolean)
					game.env.call(intstr!"jukebox_advance", null, []);
				else
				if (game.get(global!"mixhappens").to!bool.boolean)
				{
					game.env.call(intstr!"mixcontrol", null, []);
					game.runFunction("gml_Object_scorepop_obj_Create_0", new GObject, []);

					game.put(global!"mixhappens", Value(int16(0)));

					// These variables are not accessed outside the above scripts.
					if (game.get(global!"cur_day").to!int16.int16 != 0) // (exception to the above)
					{
						game.put(global!"shouldpay", Value.undefined);
						game.put(global!"rightdrink", Value.undefined);
						game.put(global!"rightdrink1", Value.undefined);
						game.put(global!"rightdrink2", Value.undefined);
						game.put(global!"big_able", Value.undefined);
						game.put(global!"big_able1", Value.undefined);
						game.put(global!"big_able2", Value.undefined);
						game.put(global!"tipping", Value.undefined);
					}

					game.env.getObject(privateObj).put(Addrs.orderDesc.name, game.get(global!"orders"));

					// Optimization: if the script we're about to execute starts with the [RES] tag,
					// which is most of the time, take advantage of this and clear the mixer variables.
					if (scriptHasRes)
						game.env.call(intstr!"resetmixer_2", null, []);
				}
				else
				if (game.visitor.get(Addrs.annaChoices).type != Value.Type.undefined)
				{
					game.visitor.inject(Addrs.annaChoiceNum, game.visitor.get(Addrs.annaChoices).array1);
					game.visitor.put(Addrs.annaChoicesCopy, game.visitor.get(Addrs.annaChoices));
					game.visitor.put(Addrs.annaChoices, Value.undefined);

					auto choice = game.visitor.get(Addrs.annaChoiceNum).int16;
					game.visitor.put(Addrs.annaChoiceStr, game.get(Addr(globalObj, IntStr("annaask%d".format(choice)))));

					auto self = new GObject;
					self.put(intstr!"image_alpha", Value(1));
					self.put(intstr!"hoversound", Value(true));
					game.runFunction("gml_Object_annachoice_%d_Step_0".format(choice), self, []);
				}
				else
				if (game.get(global!"cur_day").to!int16.int16 == -5)
					game.env.call(intstr!"annacontrol", game.textbox, []);
				else
				{
					if (!game.get(Addrs.textboxCreated).boolean) // gml_Object_dialog_control_Step_0
						game.env.call(intstr!"daycontrol", game.textbox, []);

					if (origRoom == Value(RoomID.sunday2) &&
						game.textbox.get(intstr!"room") == Value(RoomID.sunday2) &&
						!game.get(Addrs.textboxCreated).boolean)
					{
						game.visitor.inject(Addrs.todPortrait, 5.int16.iota.map!(n => Value(n)).array);
						auto todPortrait = game.visitor.get(Addrs.todPortrait);
						switch (todPortrait.int16)
						{
							case 0:  game.runFunction("gml_Object_alma_portrait_Mouse_4", null, []); break;
							case 1:  game.runFunction("gml_Object_dana_portrait_Mouse_4", null, []); break;
							case 2:  game.runFunction("gml_Object_doro_portrait_Mouse_4", null, []); break;
							case 3:  game.runFunction("gml_Object_gil_portrait_Mouse_4" , null, []); break;
							case 4:  game.runFunction("gml_Object_rad_portrait_Mouse_4" , null, []); break;
							default: assert(false);
						}
						if (!game.get(Addrs.textboxCreated).boolean)
							return false;
					}
				}

				auto roomGoto = game.get(Addrs.roomGoto);
				if (roomGoto.type != Value.Type.undefined)
				{
					game.textbox.put(intstr!"room", roomGoto);
					game.put(Addrs.roomGoto, Value.undefined);
				}

				if (game.get(Addrs.done) == Done.end)
					return true;

				auto currIter = game.save();
				if (currIter == lastIter)
					break;
				lastIter = currIter;

				// stderr.writefln("> Iterating: %-(%s, %) -> %d states",
				// 	game.visitor.stack
				// 	// .filter!(s => s.values.length > 1)
				// 	.map!(s => format("%s : %s", s.name, s.values[s.pos])),
				// 	game.visitor.currentSubset.count,
				// );
			}
			if (!game.get(Addrs.textboxCreated).boolean)
				throw new Exception("Stabilized, but textbox was not created...");
			game.put(Addrs.textboxCreated, Value.init);

			game.env.getObject(privateObj).put(Addrs.runMode.name, RunMode.script);
			break;
		}
		case RunMode.script.int32:
			playScript();
			game.env.getObject(privateObj).put(Addrs.runMode.name, RunMode.code);
			break;
		default:
			assert(false, "Unknown runMode");
	}

	return true;
}

size_t[StateSet] liveSets;
void clearStateSetCache()
{
	StateSet.clearCache();
	foreach (set, refCount; liveSets)
		set.addToCache();
}
struct ScopeSet
{
	@disable this();
	@disable this(this);
	StateSet set;
	this(StateSet set) { this.set = set; liveSets[set]++; }
	~this() { if (--liveSets[set] == 0) liveSets.remove(set); }
}
enum mixScopeSet(string set, int line = __LINE__) = `auto setScope` ~ toDec(line) ~ ` = ScopeSet(` ~ set ~ `);`;

/// forExploration = true  - We are exploring the reachable state space; don't bother saving the inputs or backups of globals
/// forExploration = false - One-off expansion of this set; the results will be discarded, and thus don't need to be culled
StateSet expand(bool forExploration)(StateSet set)
{
	mixin(mixScopeSet!q{set});

	static if (!forExploration)
	{
		// Save backups of some globals BEFORE running the script,
		// so that we can later infer which (original values of) variables
		// led the script to take which decisions.
		foreach (name; globalsToBackup.byKey)
			set = set.copyDim(Addr(globalObj, name), Addr(backupObj, name));
	}

	auto visitor = StateSetVisitor(set);

	StateSet results;
	uint n;
	void loopGame(StateSet set)
	{
		set = set
			.remove(Addrs.annaChoiceStr)
			.remove(Addrs.annaChoiceNum)
			.remove(Addrs.annaChoices)
			.remove(Addrs.annaChoicesCopy)
		;

		// Inner loop which processes the expanded states.
		game.load(set);
		while (game.next())
		{
			if (game.env.getObject(privateObj).get(Addrs.done.name) != Done.no)
				continue;

			if (!advance())
				continue;
			auto result = game.save();

			static if (forExploration)
			{
				// Cull effects
				result = result.remove(Addrs.mixertips);
				result = result.remove(Addrs.orderDesc);
				result = result.remove(Addrs.totalIngredients);
				result = result.remove(Addrs.steamAchievements);
				// Cull input
				result = result.remove(Addrs.todPortrait);
			}

			results = results.merge(result.reorderUsing(results)).normalize();
			if (n.isPowerOfTwo())
				results = results.optimize();
			if (++n % 10000 == 0)
			{
				stderr.writef("> %d: %-(%s, %) -> %d states",
					n,
					game.visitor.stack
					.filter!(s => s.values.length > 1)
					.map!(s => format("%s : %s", s.name, s.values[s.pos])),
					result.bigCount,
				); stderr.flush();
				clearStateSetCache();
				results.addToCache();
				stderr.writeln();
			}
		}
	}

	// Outer loop which decides how to expand.
	while (visitor.next())
	{
		if (visitor.get(global!"mixhappens").to!bool.boolean)
		{
			auto slotamount = visitor.get(global!"slotamount").int16;
			auto subset = visitor.currentSubset;

			// Expand with all possible drinks
			if (true)
				subset = subset.applyDrinks(drinksA, visitor);
			if (slotamount == 2)
				subset = subset.applyDrinks(drinksB, visitor);

			loopGame(subset);
		}
		else
		{
			// No expansion necessary
			loopGame(visitor.currentSubset);
		}
	}

	static if (forExploration)
	{
		// Cull var backups (only useful for condition inference)
		foreach (var; results.getDims.filter!(addr => addr.id.among(backupObj, newGlobalsObj)))
			results = results.remove(var);
	}

	return results;
}

unittest
{
	if (false)
	{
		expand!false(StateSet.unitSet);
		expand!true (StateSet.unitSet);
	}
}

StateSet expandAll(StateSet initial)
{
	StateSet next = initial;
	StateSet visited;

	uint iteration;
	while (true)
	{
		StateSet curr = next.subtract(visited);
		if (curr is StateSet.emptySet)
			break;
		visited = visited.merge(next);

		next = expand!true(curr);

		if (true)
		{
			visited = visited.optimize();
			next = next.reorderUsing(visited);
		}
		if (true)
		{
			clearStateSetCache();
			curr.addToCache();
			next.addToCache();
			visited.addToCache();
		}
		stderr.writefln("Iteration %d: Expanded %d states into %d (%d nodes, %d dims)",
			++iteration, curr.bigCount, next.bigCount, next.uniqueNodes, next.getDims.length);

		stderr.writeln(
			curr.all(Addrs.runMode).map!(v => "?CS"[v.int32]), " ", curr.all(Addrs.done).map!(v => " SE"[v.int32]), " ", curr.all(Addrs.scriptPath), " : ", curr.all(Addrs.scriptE), " -> ",
			next.all(Addrs.runMode).map!(v => "?CS"[v.int32]), " ", next.all(Addrs.done).map!(v => " SE"[v.int32]), " ", next.all(Addrs.scriptPath), " : ", next.all(Addrs.scriptE));
	}

	return visited;
}
