module va11_hall_a.dialogue.lib.graphviz;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.digest.crc;
import std.exception;
import std.format;
import std.path;
import std.range;
import std.regex;
import std.stdio;
import std.string;

import ae.sys.file;
import ae.utils.array;
import ae.utils.json;
import ae.utils.meta;
import ae.utils.path;
import ae.utils.regex;
import ae.utils.text.html;

import va11_hall_a.dialogue.lib.script;

string[] textColors;

struct ScriptPos
{
	string scriptPath;
	int e;
	mixin VisitAll; mixin AutoVisitor;
}

struct TransitionNode
{
	enum Type
	{
		condition,
		choice,
		effect,
		jump,
	}
	Type type;

	union
	{
		struct Condition
		{
			import va11_hall_a.dialogue.lib.vars : Addr; Addr addr;
			string label;
			TransitionNodePtr[] choices;
			mixin VisitAll; mixin AutoVisitor;
		}
		Condition condition;

		struct Choice
		{
			string label;
			bool obtainable;
			TransitionNodePtr next;
			mixin VisitAll; mixin AutoVisitor;
		}
		Choice choice;

		struct Effect
		{
			enum Type
			{
				mixertips,
				mixertips2,
				orderDesc,
				setVar,
				achievement,
			}

			Type type;
			union
			{
				string mixertips;
				string mixertips2;
				string orderDesc;
				string achievement;

				struct SetVar
				{
					string label, name, value;
					mixin VisitAll; mixin AutoVisitor;
				}
				SetVar setVar;
			}

			TransitionNodePtr next;

			mixin AutoVisitor;
			void visit(Handler)(Handler h) inout
			{
				if (h.handle(p => &p.type)) return;
				if (h.handle(p => &p.next)) return;
				final switch (type)
				{
					case Type.mixertips  : h.handle(p => &p.mixertips  ); return;
					case Type.mixertips2 : h.handle(p => &p.mixertips2 ); return;
					case Type.orderDesc  : h.handle(p => &p.orderDesc  ); return;
					case Type.setVar     : h.handle(p => &p.setVar     ); return;
					case Type.achievement: h.handle(p => &p.achievement); return;
				}
			}
		}
		Effect effect;

		struct Jump
		{
			ScriptPos target;
			mixin VisitAll; mixin AutoVisitor;
		}
		Jump jump;
	}

	mixin AutoVisitor;
	void visit(Handler)(Handler h) inout
	{
		if (h.handle(p => &p.type)) return;
		final switch (type)
		{
			case Type.condition: h.handle(p => &p.condition); return;
			case Type.choice   : h.handle(p => &p.choice   ); return;
			case Type.effect   : h.handle(p => &p.effect   ); return;
			case Type.jump     : h.handle(p => &p.jump     ); return;
		}
	}

	long cost; // lower is better
}

struct TransitionNodePtr
{
	TransitionNode* ptr;
	mixin AutoVisitor;
	void visit(Handler, this This)(Handler h)
	{
		static if (is(This == TransitionNodePtr))
			if (!ptr)
				ptr = new TransitionNode; // assume we're deserializing
		h.handle(p => p.ptr);
	}

	ref TransitionNode _value() { return *ptr; }
	alias _value this;
	T opCast(T : bool)() const { return !!ptr; }
}

mixin template AutoVisitor()
{
	int opCmp(ref const typeof(this)* other) const { return opCmp(*other); }
	int opCmp(ref const typeof(this) other) const
	{
		alias T = typeof(this);
		static struct Handler
		{
			T* self, other;
			int result = 0;
			bool handle(F)(scope F* delegate(T*) dg)
			{
				if (result != 0) return true;
				auto a = dg(self);
				auto b = dg(other);
				result = *a < *b ? -1 : *a > *b ? 1 : 0;
				return result != 0;
			}
		}
		auto handler = Handler(&this, &other);
		visit(&handler);
		return handler.result;
	}

	bool opEquals(ref const typeof(this)* other) const { return opEquals(*other); }
	bool opEquals(ref const typeof(this) other) const
	{
		alias T = typeof(this);
		static struct Handler
		{
			T* self, other;
			bool result = true;
			bool handle(F)(scope F* delegate(T*) dg)
			{
				if (!result) return true;
				auto a = dg(self);
				auto b = dg(other);
				result &= *a == *b;
				return !result;
			}
		}
		auto handler = Handler(&this, &other);
		visit(&handler);
		return handler.result;
	}

	hash_t toHash() const //nothrow @safe
	{
		alias T = typeof(this);
		static struct Handler
		{
			T* self;
			CRC64ISO crc;
			bool handle(F)(scope F* delegate(T*) dg)
			{
				auto a = dg(self);
				static if (is(typeof(a) == const(TransitionNodePtr)*)) // workaround DMD bug
					auto h = a.toHash();
				else
					auto h = hashOf(*a);
				crc.put(cast(ubyte[])((&h)[0..1]));
				return false;
			}
		}
		auto handler = Handler(&this);
		visit(&handler);

		static union U
		{
			typeof(handler.crc.finish()) digest;
			hash_t hash;
		}
		U u;
		u.digest = handler.crc.finish;
		return u.hash;
	}

	void toString(scope void delegate(const(char)[]) sink) const
	{
		alias T = typeof(this);
		static struct Handler
		{
			T* self;
			void delegate(const(char)[]) sink;
			bool handle(F)(scope F* delegate(T*) dg)
			{
				auto a = dg(self);
				sink.formattedWrite("%s ", *a);
				return false;
			}
		}
		auto handler = Handler(&this, sink);
		visit(&handler);
	}
}

mixin template VisitAll()
{
	void visit(Handler)(Handler h) inout
	{
		foreach (i, ref f; this.tupleof)
			if (h.handle(p => &p.tupleof[i]))
				return;
	}
}

mixin template VisitFields(fields...)
{
	void visit(Handler)(Handler h) inout
	{
		foreach (i, ref f; fields)
			if (h.handle(p => &p.tupleof[i]))
				return;
	}
}

alias Transitions = TransitionNodePtr[][ScriptPos];

string[] dump(string scriptPath, Transitions transitions, bool verbose)
{
	DayScript script = getDayScript(scriptPath);
	File f; // Current file
	string[] outFileNames;

	int counter = 1;
	struct SegmentMeta
	{
		int start;
		int end;
	}
	auto segmentMeta = new SegmentMeta[script.length];

	void needFile(int e)
	{
		if (!f.isOpen())
		{
			auto dot = scriptPath
				.rebasePath("scripts", verbose ? "dump" : "out")
				.stripExtension
				~ format("_%d.dot", e);
			outFileNames ~= dot;
			ensurePathExists(dot);

			auto bg = verbose ? "black" : "#00000000";
			f.open(dot, "wb");
			f.writefln(`digraph {`);
			f.writefln(`	graph [bgcolor=%(%s%) size="15,1000" ratio="compress"];`, [bg]);
			f.writefln(`	node [shape=box style=filled fillcolor=%(%s%) color=%(%s%) fontcolor="#FFFFFF" penwidth=2];`, [bg], [bg]);
			f.writefln(`	edge [color=white fontcolor=white fontname="helvetica"];`);
			f.writefln(`	node [fontname="monospace"];`);
			f.writefln(``);
		}
	}

	void dumpSegment(int e)
	{
		auto segment = script[e];

		if (!segment.tokens.length)
			return;

		string[] nodes;

		string s, sTerm;
		void flushTerm()
		{
			if (sTerm)
			{
				if (s.endsWith(">"))
					s ~= " "; // Hack
				s ~= sTerm;
				sTerm = null;
			}
		}

		void flushText()
		{
			flushTerm();
			if (s)
			{
				nodes ~= `[label=<%s> fillcolor="black" color="#1F1A2D" penwidth=4 margin=0.2]`.format(s ~ `<BR ALIGN="LEFT"/>`);
				s = null;
			}
		}

		foreach (token; segment.tokens)
			final switch (token.type)
			{
				case Token.Type.text:
					s ~= token.text.replace(" ", "\&nbsp;").encodeHtmlEntities; // "
					break;
				case Token.type.next:
					flushText();
					break;
				case Token.type.tag:
				{
					auto tag = token.parseTag;
					switch (tag.tag)
					{
						case "E":
						case "V": // Voice
							enforce(tag.params.length == 1);
							if (verbose)
								goto default;
							break;
						case "RES":
							//enforce(tag.params.length == 0);
							if (verbose)
								goto default;
							break;
						case "STOPLIP":
							enforce(tag.params.length == 0);
							break;
						case "XS":
							enforce(tag.params.length == 2);
							switch (tag.params[0])
							{
								case "ph":
								case "mix":
									if (verbose)
										goto default;
									break;
								default:
									if (tag.params[0].endsWith("talk") || tag.params[0].among("next"))
										break;
									// segmentMeta[e].vars[tag.params[0]] = tag.params[1];
									flushText();
									nodes ~= `[label=%(%s%) fillcolor="#331122" color="#664455" fontcolor="#EECCDD"]`
									// 	.format(tag.params.join(" := ").only));
										.format((tag.tag ~ tag.params).join("\n").only);
									break;
							}
							break;
						case "C":
							enforce(tag.params.length == 1);
							flushTerm();
							if (tag.params[0] == "C")
							{ /* Flush only */ }
							else
							{
								auto c = textColors.get(tag.params[0].to!size_t, null);
								if (c)
								{
									s ~= `<FONT COLOR="%s">`.format(c);
									sTerm = `</FONT>`;
								}
								else
								{
									s ~= `<B>`;
									sTerm = `</B>`;
								}
							}
							break;
						default:
							flushText();
							nodes ~= `[label=%(%s%) fillcolor="#222222" color="#555555" fontcolor="#DDDDDD"]`
								.format((tag.tag ~ tag.params).join("\n").only);
							break;
					}
					break;
				}
				case Token.type.lineBreak:
					s ~= `<BR ALIGN="LEFT"/>`;
					break;
			}

		flushText();

		if (!verbose && nodes.length >= 9)
			nodes =
				nodes[0 .. 3] ~
				[`[label=%(%s%)]`.format("·····\n%d lines\nomitted\n·····".format(nodes.length - 6).only)] ~
				nodes[$ - 3 .. $];

		foreach (node; nodes)
		{
			if (!segmentMeta[e].start)
				segmentMeta[e].start = counter;
			else
				f.writefln("\t%d -> %d;", counter - 1, counter);
			segmentMeta[e].end = counter;
			f.writefln("\t%d %s", counter, node);
			counter++;
		}
	}

	int[int] missingENodes;
	SegmentMeta needSegment(int e)
	{
		if (e < script.length && script[e].tokens.length)
		{
			if (segmentMeta[e] is SegmentMeta.init)
			{
				dumpSegment(e);
				assert(segmentMeta[e] !is SegmentMeta.init);
			}
			return segmentMeta[e];
		}
		auto n = missingENodes.require(
			e,
			{
				needFile(e);
				switch (e)
				{
					case E.start:
					case E.start2:
						f.writefln("\t%d %s", counter, `[label="START"]`);
						break;
					case E.end:
						f.writefln("\t%d %s", counter, `[label="END"]`);
						break;
					default:
						if (e < 0)
							f.writefln("\t%d %s", counter, `[shape=point]`);
						else
							f.writefln("\t%d %s", counter, `[label=%(%s%)]`.format(
								["Missing E: " ~ text(e)],
							));
				}
				return counter++;
			}(),
		);
		return SegmentMeta(n, n);
	}

	// Extra script rules
	string[] scriptRules;
	HashSet!size_t ruleAdded;
	size_t[][int] ruleLookup;
	int[][] ruleEs;

	string parseRule(string rule, SegmentMeta delegate(int) resolveLocation)
	{
		int parseLocation(string loc)
		{
			if (loc.skipOver("s"))
				return resolveLocation(loc.to!int).start;
			else
			if (loc.skipOver("e"))
				return resolveLocation(loc.to!int).end;
			else
				throw new Exception("Unknown location: " ~ loc);
		}

		return rule.replaceAll!(m => parseLocation(m[1]).to!string)(re!`\$(\w+)\b`);
	}

	{
		scriptRules = extraRules.get(scriptPath, null);
		ruleEs.length = scriptRules.length;
		foreach (ruleIndex, rule; scriptRules)
		{
			int parseLocation(string loc)
			{
				int addE(int e) { ruleEs[ruleIndex] ~= e; return e; }
				if (loc.skipOver("s"))
					return needSegment(loc.to!int.I!addE).start;
				else
				if (loc.skipOver("e"))
					return needSegment(loc.to!int.I!addE).end;
				else
					throw new Exception("Unknown location: " ~ loc);
			}

			parseRule(rule, (e) { ruleEs[ruleIndex] ~= e; return SegmentMeta.init; });
			foreach (e; ruleEs[ruleIndex])
				ruleLookup[e] ~= ruleIndex;
		}
	}

	int[TransitionNodePtr] visited;
	HashSet!int sourceVisited;
	int[int][string] continuedNodes;

	void visitSource(ScriptPos source)
	{
		if (source.scriptPath != scriptPath)
			return;
		if (source.e in sourceVisited)
			return;
		sourceVisited.add(source.e);
		needFile(source.e);

		void visit(TransitionNodePtr node, int start)
		{
			if (auto ptarget = node in visited)
			{
				f.writefln("\t%d -> %d;", start, *ptarget);
				return;
			}
			visited[node] = counter; // assume at least one node will be created

			final switch (node.type)
			{
				case TransitionNode.Type.condition:
				{
					auto p = start;

					if (node.condition.label)
					{
						f.writefln("\t%d -> %d;", p, counter);
						f.writefln("\t" ~ `%d [fixedsize=true width=%s height=%s labelloc=b label=%(%s%) fillcolor="#666600" color="#aaaa00" fontcolor="#FFFFCC" shape=house]`,
							counter,
							node.condition.label.split("\n").map!(l => l.length).reduce!max * 0.13 + 0.1,
							node.condition.label.split("\n").length                         * 0.3  + 0.3,
							[node.condition.label]);
						p = counter++;
					}

					foreach (choice; node.condition.choices)
						visit(choice, p);

					break;
				}

				case TransitionNode.Type.choice:
				{
					auto p = start;

					f.writefln("\t%d -> %d [style=%s];", p, counter,
						node.choice.obtainable ? "solid" : "dotted");
					string colors = node.choice.label == "otherwise"
						? `fillcolor="#665500" color="#aa9900" fontcolor="#ffeecc"`
						: `fillcolor="#556600" color="#99aa00" fontcolor="#eeffcc"`;
					auto label = node.choice.label;
					auto lines = label.split("\n");
					auto width  = lines.map!(l => l.length).reduce!max;
					auto height = lines.length;
					if (lines.length > 2)
						while (height > 1 && lines[height - 2].length > 2 + lines[height - 1].length)
						{
							label = "\n" ~ label;
							height--;
						}
					label ~= "\n".replicate(cast(int)(1.5 + height * 0.4));
					f.writefln("\t" ~ `%d [fixedsize=true width=%s height=%s label=%(%s%) %s shape=invhouse]`,
						counter,
						width  * 0.13   + 0.1,
						height * 0.3675 + 0.3,
						[label], colors);
					p = counter++;

					visit(node.choice.next, p);
					break;
				}

				case TransitionNode.Type.effect:
				{
					auto p = start;

					final switch (node.effect.type)
					{
						case TransitionNode.Effect.Type.mixertips:
							f.writefln("\t%d -> %d;", p, counter);
							f.writefln("\t" ~ `%d [label=%(%s%) %s]`,
								counter, [node.effect.mixertips],
								node.effect.mixertips == "Wrong drink!"
								? `fillcolor="#440000" color="#880000" fontcolor="#FFAAAA"`
								: `fillcolor="#113322" color="#446655" fontcolor="#CCFFDD"`
							);
							p = counter++;
							break;
						case TransitionNode.Effect.Type.mixertips2:
							f.writefln("\t%d -> %d;", p, counter);
							f.writefln("\t" ~ `%d [margin="0" border="0" label=<%s>]`,
								counter, node.effect.mixertips2);
							p = counter++;
							break;
						case TransitionNode.Effect.Type.orderDesc:
							f.writefln("\t%d -> %d;", p, counter);
							f.writefln("\t" ~ `%d [label=%s fillcolor="#111133" color="#444466" fontcolor="#CCCCFF" margin=0.2]`,
								counter, node.effect.orderDesc.only.format!"%(%s%)".replace(`\n`, `\l`));
							p = counter++;
							break;
						case TransitionNode.Effect.Type.setVar:
							f.writefln("\t%d -> %d;", p, counter);
							f.writefln("\t" ~ `%d [label=%(%s%) fillcolor="#221133" color="#554466" fontcolor="#DDCCEE"]`,
								counter, [node.effect.setVar.label ? node.effect.setVar.label : format("%s := %s", node.effect.setVar.name, node.effect.setVar.value)]);
							p = counter++;
							break;
						case TransitionNode.Effect.Type.achievement:
							f.writefln("\t%d -> %d;", p, counter);
							f.writefln("\t" ~ `%d [label=%(%s%) fillcolor="#222233" color="#555566" fontcolor="#DDDDEE"]`,
								counter, ["Achievement\nunlocked:\n" ~ node.effect.achievement]);
							p = counter++;
							break;
					}

					visit(node.effect.next, p);
					break;
				}

				case TransitionNode.Type.jump:
				{
					auto p = start;

					if (node.jump.target.scriptPath == scriptPath)
					{
						if (node.jump.target.e == 0)
							return;
						auto target = needSegment(node.jump.target.e).start;
						f.writefln("\t%d -> %d;", p, target);
						visited[node] = target;

						// Map out entire subgraph recursively here
						visitSource(node.jump.target);
					}
					else
					{
						auto n = continuedNodes.require(node.jump.target.scriptPath, null).require(node.jump.target.e,
							{
								f.writefln("\t%d %s", counter, `[label=%(%s%)]`.format(
									["Continued in\n%s\nE%d".format(relPath(node.jump.target.scriptPath, source.scriptPath.dirName), node.jump.target.e)],
								));
								return counter++;
							}(),
						);
						f.writefln("\t%d -> %d;",
							p,
							n,
						);
					}
					break;
				}
			}
		}

		foreach (node; transitions.get(source, null))
			visit(node, needSegment(source.e).end);

		foreach (ruleIndex; ruleLookup.get(source.e, null))
			if (ruleIndex !in ruleAdded)
			{
				auto rule = scriptRules[ruleIndex];
				rule = parseRule(rule, e => needSegment(e));
				f.writefln("\t%s;", rule);
				foreach (e; ruleEs[ruleIndex])
					visitSource(ScriptPos(scriptPath, e));
				ruleAdded.add(ruleIndex);
			}
	}

	int eOrder(int e)
	{
		if (e == 26 && scriptPath.endsWith("script13.txt"))
			return -1;
		switch (e)
		{
			case E.start:
			case E.start2:
				return e;
			case E.end:
				return int.max;
			default:
				if (e < 0)
					return -e - 1000;
				return e;
		}
	}

	foreach (source; transitions.keys.sort!((a, b) => eOrder(a.e) < eOrder(b.e)))
	{
		visitSource(source);

		if (f.isOpen)
		{
			// Subgraph fully explored.
			f.writeln("}");
			f.close();
		}
	}

	bool tokenIsMeaningful(Token t) { return t.type != Token.Type.tag || !t.parseTag.tag.among("E", "STOPLIP"); }

	foreach (e, segment; script)
		if (!segment.tokens.filter!tokenIsMeaningful.empty && segmentMeta[e] is SegmentMeta.init)
			throw new Exception("Script segment not visited: %s @ %d".format(scriptPath, e));

	foreach (i, rule; scriptRules)
		if (i !in ruleAdded)
			throw new Exception("Rule %d was not added: %s (%s)".format(i, extraRules[scriptPath][i], rule));

	return outFileNames;
}

string[][string] extraRules;

static this()
{
	foreach (line; File("graph-extras.txt", "rb").byLine)
	{
		auto parts = line.split("\t");
		enforce(parts.length == 2);
		extraRules[parts[0].idup] ~= parts[1].idup;
	}
}
