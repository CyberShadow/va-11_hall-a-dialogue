module va11_hall_a.dialogue.lib.constants;

/// For some reason, Altar.NET does not produce this list anywhere in its output...
enum ObjID : short
{
	dialog_control          =  43,
	obj_textbox             =  60,
	credits_start           =  61,
	log_obj                 =  79,
	shaker_a                = 106,
	out_to_title            = 293,
	out_to_bar              = 296,
	out_to_codec            = 300,
	out_to_codecload2       = 301,
	out_to_balcony          = 302,
	out_to_dareload         = 303,
	out_to_break            = 305,
	out_to_gameover         = 307,
	out_to_results          = 308,
	out_to_end              = 310,
	new_day                 = 313,
	out_of_apartment        = 318,
	cuttoblack_intro        = 322,
	save_home               = 378,
	room_settings           = 453,
	jillroom_exit           = 454,
	popup_room              = 458,
	break_savereturn        = 471,
	break_savehome          = 472,
	breakend_text           = 473,
	jill_portrait           = 478,
	dana_portrait           = 480,
	moving_frame            = 485,
	dana_portrait_codec     = 494,
	alma_portrait_codec     = 496,
	sei_portrait            = 498,
	top_wordbanner          = 503,
	bottom_wordbanner       = 504,
	text_control_codec      = 509,
	break_bumper            = 560,
	intro1_text             = 569,
	annachoice_1            = 627,
	annachoice_2            = 628,
	annachoice_3            = 629,
	annachoice_4            = 630,
	annachoice_5            = 631,
	annachoice_6            = 632,
	annachoice_7            = 633,
	annachoice_8            = 634,
	annachoice_9            = 635,
	anna_out                = 649,
}

enum RoomID : short
{
	jill_room       = 9,
	bar             = 10,
	break_time      = 11,
	sunday1         = 23,
	sunday2         = 24,
	week1_intro     = 25,
	week2_intro     = 26,
	week3_intro     = 27,
	codec_room      = 29,
	codecload2      = 35,
}

enum MixerTipsArgs
{
	shouldpay,
	rightdrink,
	big_able,
	tipping,
}

enum MixerTips2Args
{
	shouldpay,
	rightdrink1,
	rightdrink2,
	big_able1,
	big_able2,
	tipping,
}
