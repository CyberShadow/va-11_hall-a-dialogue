module va11_hall_a.dialogue.lib.colors;

import std.file;
import std.format;

import gml_lsp_emu;

string[] getColors()
{
	class ColorEnv : Env
	{
		Value call(IntStr name, IObject self, Value[] arguments)
		{
			if (name == intstr!"draw_set_font")
				return Value.init;
			else
			if (name == intstr!"string_height")
				return Value(10);
			else
			if (name == intstr!"string_width")
				return Value(100);
			else
			if (name == intstr!"surface_create")
				return Value.init;
			else
			if (name == intstr!"action_move_start")
				return Value.init;
			else
			if (name == intstr!"action_reverse_ydir")
				return Value.init;
			else
				throw new Exception(format("Unknown function call: %s(%(%s, %))", name, arguments));
		}

		override Env create() const { return new ColorEnv; }
	}

	auto self = new GObject;
	self.put(intstr!"view_hport", Value([Value(0)]));
	self.put(intstr!"view_wport", Value([Value(0)]));
	self.put(intstr!"room", Value(short(0)));

	Env env = new ColorEnv;
	// env.getGlobal().put(intstr!"fnt_textbox", Value.void_);
	Value runFile(string path)
	{
		return run(parse(readText(path), path), env, self, []);
	}

	runFile("decompiled/code/gml_Object_obj_textbox_Create_0.gml.lsp");

	string[] colors;
	foreach (i; 0 .. 100)
	{
		self.put(intstr!"argument0", Value(i));
		auto r = runFile("decompiled/code/gml_Script_textbox_color_get.gml.lsp");
		auto c = r.to!int.get!int;
		colors ~= format("#%02X%02X%02X", c & 0xFF, (c >> 8) & 0xFF, c >> 16);
	}
	return colors;
}
