module va11_hall_a.dialogue.lib.script;

import std.algorithm.searching;
import std.conv;
import std.exception;
import std.file;
import std.string;

import ae.utils.array;

struct Token
{
	enum Type
	{
		tag,
		text,
		next,
		lineBreak,
	}
	Type type;

	string text;

	struct Tag
	{
		string tag;
		string[] params;
	}
	Tag parseTag() const
	{
		assert(type == Type.tag);
		string s = text;
		Tag tag;
		tag.tag = s.skipUntil(':');
		tag.params = s.split(',');
		return tag;
	}
}

Token[] lex(string s)
{
	s.skipOver("\uFEFF");

	Token[] tokens;
	while (s.length)
	{
		auto c = s.shift;
		Token token;
		switch (c)
		{
			case '[':
			{
				token.type = Token.Type.tag;
				token.text = s.skipUntil(']');
				break;
			}
			case '\r':
				s.skipOver("\n").enforce();
				token.type = Token.Type.next;
				break;
			case '\n':
				assert(false);
			case '#':
				token.type = Token.Type.lineBreak;
				break;
			default:
			{
				token.type = Token.Type.text;
				auto p = s.representation.countUntil!(c => c == '\r' || c == '\n' || c == '#' || c == '[');
				if (p < 0)
					p = s.length;
				token.text = c ~ s[0..p];
				s = s[p..$];
				break;
			}
		}
		tokens ~= token;
	}
	return tokens;
}

struct Segment
{
	Token[] tokens;
	int next; // E
}

alias DayScript = Segment[/*E*/];

DayScript parseDay(Token[] tokens)
{
	DayScript day;
	{
		int currentE = 0;
		day.length = 1;

		foreach (token; tokens)
		{
			if (token.type == Token.Type.tag)
			{
				auto tag = token.parseTag();
				if (tag.tag == "E")
				{
					auto newE = tag.params[0].to!int;
					if (newE != currentE)
					{
						day[currentE].next = newE;
						currentE = newE;
						if (day.length < currentE + 1)
							day.length = currentE + 1;
						enforce(!day[currentE].tokens, "Duplicate E-label: " ~ tag.params[0]);
					}
				}
			}
			day[currentE].tokens ~= token;
		}
	}
	return day;
}

DayScript[string] dayScripts;
DayScript getDayScript(string path)
{
	return dayScripts.require(path,
		path
		.readText
		.lex
		.parseDay
	);
}

// Internal E values
enum E : short
{
	start = short.min,
	start2,
	end,
}
