module va11_hall_a.dialogue.lib.vars;

import ae.utils.aa;

import std.algorithm.iteration;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.file;
import std.format;
import std.stdio;
import std.string;
import std.traits;

import gml_lsp_emu.intstr;
import gml_lsp_emu.program : Value;
import gml_lsp_emu.parser : parseValue;

import va11_hall_a.dialogue.lib.serialization;

struct Addr
{
	int id;
	IntStr name;

	void toString(scope void delegate(const(char)[]) sink) const
	{
		switch (id)
		{
			case privateObj:
				switch (name.index)
				{
					static foreach (i, m; EnumMembers!Addrs)
					{
						case m.name.index:
							sink(__traits(identifier, EnumMembers!Addrs[i]));
							return;
					}
					default:
				}
				break;
			case steamAchievementsObj: sink.formattedWrite!"steam-achievements.%s"(name); break;
			case backupObj           : sink.formattedWrite!"backup.%s"            (name); break;
			case globalObj           : sink.formattedWrite!"global.%s"            (name); break;
			case newGlobalsObj       : sink.formattedWrite!"new-globals.%s"       (name); break;
			default                  : sink.formattedWrite!"%d.%s"(id, name);
		}
	}

	int opCmp(ref const Addr s) const
	{
		return name == s.name ? s.id - id : cmp(name.toString(), s.name.toString());
	}
}

// Object ID for private vars (so that we can reuse Addr to store values not accessible by the game script)
enum privateObj = int.min;
enum steamAchievementsObj = privateObj + 1;
enum backupObj            = privateObj + 2;
enum newGlobalsObj        = privateObj + 3;
enum globalObj = 1;

enum Addrs
{
	scriptPath        = Addr(privateObj, IntStr( 0)),
	scriptE           = Addr(privateObj, IntStr( 1)),
	done              = Addr(privateObj, IntStr( 2)),
	mixertips         = Addr(privateObj, IntStr( 6)),
	edgeIndex         = Addr(privateObj, IntStr( 7)),
	orderDesc         = Addr(privateObj, IntStr( 8)),
	runMode           = Addr(privateObj, IntStr( 9)), // See RunMode
	textboxCreated    = Addr(privateObj, IntStr(10)),
	totalIngredients  = Addr(privateObj, IntStr(11)),
	roomGoto          = Addr(privateObj, IntStr(12)), // Queued room_goto
	todPortrait       = Addr(privateObj, IntStr(13)),
	annaChoiceNum     = Addr(privateObj, IntStr(14)),
	annaChoiceStr     = Addr(privateObj, IntStr(15)),
	annaChoices       = Addr(privateObj, IntStr(16)),
	annaChoicesCopy   = Addr(privateObj, IntStr(17)),
	origScriptPath    = Addr(privateObj, IntStr(18)),
	origScriptE       = Addr(privateObj, IntStr(19)),
	steamAchievements = Addr(privateObj, IntStr(20)),
	gameOver          = Addr(privateObj, IntStr(21)),
}

auto global(string s)() { return Addr(globalObj, intstr!s); }
auto backup(string s)() { return Addr(backupObj, intstr!s); }

enum RunMode /// What should run next?
{
	code   = Value(1),
	script = Value(2),
}

enum Done
{
	no     = Value(0), // Not done
	sync   = Value(1), // Break, or end of day - sync point
	end    = Value(2), // Game Over
}

// ****************************************************************************

/// Optimize execution by discarding writes to variables that will never be read,
/// or reads of variables that are only written once (during initialization, i.e., constants).
HashSet!Addr willWrite, willRead;
bool varsUpdated;

/// Back up globals that look like they (may) influence decision-making.
HashSet!IntStr globalsToBackup;
bool globalsUpdated;

/// Possible values for vars where we need to know all values
HashSet!Value[IntStr] varValues;
bool varValuesUpdated;

static this()
{
	HashSet!Addr loadSet(string fn)
	{
		HashSet!Addr result;
		if (!fn.exists)
			return result;
		foreach (line; fn.readText.splitLines)
		{
			if (!line.length)
				continue;
			auto parts = line.findSplit(".");
			Addr addr;
			addr.id = parts[0].to!int;
			addr.name = IntStr(parts[2]);
			result.add(addr);
		}
		return result;
	}

	willWrite = loadSet("vars-write.txt");
	willRead  = loadSet("vars-read.txt");

	if ("vars-globals.txt".exists)
		globalsToBackup = File("vars-globals.txt")
			.byLine
			.map!(s => s.chomp.idup.IntStr)
			.toSet;
}

static ~this()
{
	bool needRerun;

	if (varsUpdated)
	{
		void saveSet(HashSet!Addr set, string fn)
		{
			if (set.empty)
				return;
			import std.file : write;
			write(fn, set
				.keys
				.sort
				.map!(addr => format("%d.%s\n", addr.id, addr.name))
				.join
			);
		}

		saveSet(willWrite, "vars-write.txt");
		saveSet(willRead , "vars-read.txt");
		needRerun = true;
	}

	if (globalsUpdated)
	{
		globalsToBackup.byKey.map!(s => s.toString() ~ "\n").array.sort.toFile("vars-globals.txt");
		needRerun = true;
	}

	if (varValuesUpdated)
	{
		foreach (name, values; varValues)
		{
			auto fn = "vars-values-" ~ name.toString() ~ ".txt";
			import std.file : write;
			write(fn, values
				.keys
				.sort
				.map!(v => format("%s\n", v))
				.join
			);
		}
		needRerun = true;
	}

	if (needRerun)
		throw new Exception("Var definitions updated, re-run required");
}

void checkVar(ref HashSet!Addr set, Addr addr, string operation)
{
	if (addr in set)
		return;
	import std.stdio : stderr;
	stderr.writefln("UNEXPECTED %s: %s", operation, addr);
	set.add(addr);
	varsUpdated = true;
}
void checkWrite(Addr addr) { willWrite.checkVar(addr, "WRITE"); }
void checkRead (Addr addr) { willRead .checkVar(addr, "READ" ); }

void useGlobal(IntStr name)
{
	if (name !in globalsToBackup)
	{
		globalsToBackup.add(name);
		import std.stdio : stderr;
		stderr.writefln("UNEXPECTED GLOBAL: %s", name);
		globalsUpdated = true;
	}
}

ref HashSet!Value getVarValues(IntStr name)
{
	return varValues.require(name, {
		HashSet!Value result;
		auto fn = "vars-values-" ~ name.toString() ~ ".txt";
		if (!fn.exists)
			return result;
		foreach (line; fn.readText.splitLines)
		{
			if (!line.length)
				continue;
			result.add(line.parseValue());
		}
		return result;
	}());
}

void checkVarValue(IntStr name, Value value)
{
	auto set = &getVarValues(name);
	if (value !in *set)
	{
		import std.stdio : stderr;
		stderr.writefln("UNEXPECTED VALUE OF %s: %s", name, value);
		set.add(value);
		varValuesUpdated = true;
	}
}

/// Persistent vars that are independent of the normal, temporary or
/// game progress vars.  These are saved outside of the normal save files.
HashSet!Addr persistentVars;
static this()
{
	persistentVars = [
		// Configuration
		global!"ngplus_able",
		global!"can_truth",

		// Anna
		global!"annaquestion1",
		global!"annaquestion2",
		global!"annaquestion3",
		global!"annaquestion4",
		global!"annaquestion5",
		global!"annaquestion6",
		global!"annaquestion7",
		global!"annaquestion8",
		global!"annaquestion9",

		// This one is actually saved in save files, but it's simpler to put this here.
		global!"ngplus_flag",
	].toSet();
}

// ****************************************************************************

// Keep track of what variables end up (being shown) as used where,
// to allow creating a nice index.

struct VarSighting
{
	string path; int e;
	IntStr name;
	bool write;
}

HashSet!VarSighting loadSightings()
{
	enum fn = "vars.sightings";
	HashSet!VarSighting sightings;
	if (fn.exists)
		sightings = load!(VarSighting[])(File(fn, "rb")).toSet;
	return sightings;
}

void saveSightings(HashSet!VarSighting sightings) /// Merge with existing set
{
	enum fn = "vars.sightings";
	auto allSightings = loadSightings();
	foreach (sighting; sightings)
		allSightings.add(sighting);
	allSightings.keys.save(File(fn, "wb"));
}
