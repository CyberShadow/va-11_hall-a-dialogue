#!/bin/bash
set -eEuo pipefail

function group_of() {
	local fn=$1

	local prefix="${fn:0:8}"

	if [[ "$prefix" == 'state_0,' ]]
	then
		printf -- 'state_1,'
	else
		printf -- '%s' "$prefix"
	fi
}

function loop() {
	local -A seen

	local group=()
	local fn last_fn

	while IFS= read -r fn
	do
		if [[ -v seen[$fn] ]]
		then
			continue
		fi
		seen[$fn]=_

		if [[ -v last_fn && "$(group_of "$fn")" != "$(group_of "$last_fn")" ]]
		then
			./graph "${group[@]}" # &>/dev/null &
			group=()
		fi

		if ! ./explore "$fn"
		then
			# Retry one more time
			./explore "$fn"
		fi

		group+=("${fn%%.set}.points")

		last_fn=$fn
	done < states.txt

	./graph "${group[@]}"

	wait
}

function loop_continue() {
	# Continue from specified states

	rm -f states.txt

	for f in "$@"
	do
		printf -- '%s\n' "$f"
	done > states.txt

	loop
}

function loop_new() {
	# Start from scratch

	find . -maxdepth 1 -name '*.set' -delete
	find . -maxdepth 1 -name '*.points' -delete
	find . -maxdepth 1 -name '*.transitions' -delete

	cp ./-/2020-09-26b/*.set ./
	cp ./-/2020-09-26b/*.points ./

	./get_drinks
	./gen_seeds

	# Process seeds group-by-group too
	local group=()
	local fn last_fn

	local states
	mapfile -t states < <(sort states.txt)
	for fn in "${states[@]}"
	do
		if [[ -v last_fn && "$(group_of "$fn")" != "$(group_of "$last_fn")" ]]
		then
			loop_continue "${group[@]}"
			group=()
		fi

		group+=("$fn")
		last_fn=$fn
	done
	loop "${group[@]}"
}

if [[ $# -gt 0 ]]
then
	loop_continue "$@"
else
	loop_new
fi
