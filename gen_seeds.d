module va11_hall_a.dialogue.gen_seeds;

import std.format;
import std.stdio;

import gml_lsp_emu.intstr;
import gml_lsp_emu.program;

import va11_hall_a.dialogue.lib.game;
import va11_hall_a.dialogue.lib.stateset;
import va11_hall_a.dialogue.lib.serialization;
import va11_hall_a.dialogue.lib.vars;

File states;

void saveSet(StateSet set, string fileName)
{
	stderr.writefln("Saving %s ...", fileName);
	set.optimize3.save(File(fileName, "wb"));
	states.writeln(fileName);
}

void saveSeed(short day, short stage, short client)
{
	StateSet state = initialState;
	assert(state.count == 1);
	state = state.set(global!"cur_day"   , Value(day));
	state = state.set(global!"cur_stage" , Value(stage));
	state = state.set(global!"cur_client", Value(client));
	state = state.set(Addrs.runMode, RunMode.code);

	foreach (var; persistentVars)
	{
		assert(var.id == globalObj);
		auto values = getVarValues(var.name);
		if (!values.empty)
			state = state.cartesianProduct(var, values.keys);
	}

	saveSet(state, "state_%d,%d,%d.set".format(day, stage, client));
}

void saveGameOver()
{
	auto state = initialState;

	state = state.cartesianProduct(global!"cur_day"   , getVarValues(intstr!"cur_day"   ).keys);
	state = state.cartesianProduct(global!"cur_stage" , getVarValues(intstr!"cur_stage" ).keys);
	state = state.cartesianProduct(global!"cur_client", getVarValues(intstr!"cur_client").keys);

	state = state.set(Addrs.gameOver, Value(true));
	state = state.set(Addrs.runMode , RunMode.code);

	saveSet(state, "state_gameover.set");
}

void main()
{
	initGame();

	states = File("states.txt", "wb");

	saveSeed(-2, 1, 1); // Prologue, Day 1
	saveSeed( 0, 1, 1); // Anna Demo
	saveSeed(-5, 1, 1); // Anna Start
	saveSeed( 0, 0, 1); // New Game

	saveGameOver();
}
